<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Purchase extends Model
{
	protected $fillable = [
        'total_cost', 'payment_status', 'date',
    ];


    public function purchaseOrderLines(){
    	return $this->hasMany('App\PurchaseOrderLine');
    }
}
