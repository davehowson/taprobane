<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PurchaseOrderLine extends Model
{
	protected $fillable = [
        'product_id', 'purchase_id', 'vendor_id', 'unit_price', 'quantity',
    ];


    public function purchase(){
    	return $this->belongsTo('App\Purchase');
    }

    public function vendor(){
    	return $this->belongsTo('App\Vendor');
    }

     public function product(){
    	return $this->belongsTo('App\Product');
    }
}
