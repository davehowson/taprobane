<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Delivery extends Model
{
	protected $fillable = [
        'address_id', 'delivery_status',
    ];

    public function invoice(){
    	$this->belongsTo('App\Invoice');
    }

     public function deliverylog()
    {
        return $this->hasMany('App\DeliveryLog');
    }
     
}
