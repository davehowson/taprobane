<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class UpdateVendor extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => 'nullable',
            'contact_number' => 'nullable',
            'address' => 'nullable',
            'credit_period' => 'nullable',
            'rating' => 'nullable|integer|min:-10|max:10',
        ];
    }

    public function persist()
    {
        $this->user()->authorizeRoles('Administrator');
        $vendor = \App\Vendor::findOrFail(request('vendor'));

        if (!($this->input('name')=='')){
            $vendor->name = $this->input('name');
        }

        if (!($this->input('contact_number')=='')){
            $vendor->contact_number = $this->input('contact_number');
        }

        if (!($this->input('credit_period')=='')){
            $vendor->credit_period = $this->input('credit_period');
        }

        if (!($this->input('rating')=='')){
            $vendor->rating = $this->input('rating');
        }


        $vendor->save();

        if (!($this->input('billing-address-one') == '')){


            $vendor->addresses()->where('isBilling', '1')->update([
                'address_one'=>request('billing-address-one'),
                'address_two'=>request('billing-address-two'),
                'city'=>request('billing-city'),
                'state'=>request('billing-state'),
                'postal_code'=>request('billing-postal-code'),
                'country'=>request('billing-country'),
                'isBilling'=>'1',                
            ]);
            if (request('isDelivery') == false) {
                     $vendor->addresses()->where('isBilling', '1')->update(['isDelivery'=>'0']);
                } elseif (request('isDelivery') == true) {
                    $vendor->addresses()->where('isDelivery', '1')->where('isBilling', '0')->delete();
                    $vendor->addresses()->where('isBilling', '1')->update(['isDelivery'=>'1']);
                }
        }
        

        if (request('isDelivery') == false) {


            //$vendor->addresses()->where('isDelivery', '1')->where('isBilling', '0')->delete();


            $vendor->addresses()->updateOrCreate(
                ['isDelivery' => '1'],
                ['address_one'=>request('delivery-address-one'),
                'address_two'=>request('delivery-address-two'),
                'city'=>request('delivery-city'),
                'state'=>request('delivery-state'),
                'postal_code'=>request('delivery-postal-code'),
                'country'=>request('delivery-country'),
                'isBilling'=>'0',]
            );
        }
    }

}
