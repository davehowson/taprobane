<?php

namespace App\Http\Requests;

use App\Customer;
use Illuminate\Foundation\Http\FormRequest;

class CreateCustomer extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => 'required',
            'email' => 'required|email',
            'password' => 'required|confirmed|min:8',

            'billing-address-one'=>'required',
            'billing-address-two'=>'required',
            'billing-city' => 'required',
            'billing-postal-code' => 'required',
            'billing-country' => 'required',
        ];
    }

    public function persist(){
        $customer = Customer::create([
            'name' => request('name'),
            'email' => request('email'),
            'password' => bcrypt(request('password')),

        ]);

        $customer->addresses()->create([
            'address_one'=>request('billing-address-one'),
            'address_two'=>request('billing-address-two'),
            'city'=>request('billing-city'),
            'state'=>request('billing-state'),
            'postal_code'=>request('billing-postal-code'),
            'country'=>request('billing-country'),
            'isBilling'=>'1',
            'isDelivery'=>request('isDelivery'),

        ]);

        if (request('isDelivery') == false) {
            $customer->addresses()->create([
            'address_one'=>request('delivery-address-one'),
            'address_two'=>request('delivery-address-two'),
            'city'=>request('delivery-city'),
            'state'=>request('delivery-state'),
            'postal_code'=>request('delivery-postal-code'),
            'country'=>request('delivery-country'),
            'isBilling'=>'0',
            'isDelivery'=>'1',

        ]);
        }
    }
}
