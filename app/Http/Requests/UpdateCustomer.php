<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class UpdateCustomer extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => 'nullable',
            'email' => 'nullable|email',
            'password' => 'nullable|min:8|confirmed',
        ];
    }

    public function persist()
    {
        $this->user()->authorizeRoles('Administrator');
        $customer = \App\Customer::findOrFail(request('customer'));

        if (!($this->input('name')=='')){
            $customer->name = $this->input('name');
        }

        if (!($this->input('email')=='')){
            $customer->email = $this->input('email');
        }

        if (!($this->input('password')=='')){
            $customer->password = bcrypt($this->input('password'));
        }


        $customer->save();

        if (!($this->input('billing-address-one') == '')){


            $customer->addresses()->where('isBilling', '1')->update([
                'address_one'=>request('billing-address-one'),
                'address_two'=>request('billing-address-two'),
                'city'=>request('billing-city'),
                'state'=>request('billing-state'),
                'postal_code'=>request('billing-postal-code'),
                'country'=>request('billing-country'),
                'isBilling'=>'1',                
            ]);
            if (request('isDelivery') == false) {
                     $customer->addresses()->where('isBilling', '1')->update(['isDelivery'=>'0']);
                } elseif (request('isDelivery') == true) {
                    $customer->addresses()->where('isDelivery', '1')->where('isBilling', '0')->delete();
                    $customer->addresses()->where('isBilling', '1')->update(['isDelivery'=>'1']);
                }
        }
        

        if (request('isDelivery') == false) {


            //$customer->addresses()->where('isDelivery', '1')->where('isBilling', '0')->delete();


            $customer->addresses()->updateOrCreate(
                ['isDelivery' => '1'],
                ['address_one'=>request('delivery-address-one'),
                'address_two'=>request('delivery-address-two'),
                'city'=>request('delivery-city'),
                'state'=>request('delivery-state'),
                'postal_code'=>request('delivery-postal-code'),
                'country'=>request('delivery-country'),
                'isBilling'=>'0',]
            );
        }
    }
}
