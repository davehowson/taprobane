<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use App\Product;
use App\Category;
use Illuminate\Support\Facades\Storage;

class CreateProduct extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'product_name' => 'required',
            'product_description' => 'required',
            'product_price' => 'required|numeric',
            'product_category' => 'required',
        ];
    }

    public function persist(){

        if ($this->hasFile('product_image'))
        {
            $path = Storage::putFile('public/products', request('product_image'));

            $path = explode('/',$path);

            $path = $path[2];
        }
        else 
        {
            $path = "product-image-placeholder.png";
        }

        

        $product = Product::create([
            'product_name' => request('product_name'),
            'product_description' => request('product_description'),
            'product_price' => request('product_price'),
            'product_image' => "$path",
            'category_id' => request('product_category'),
        ]);


        $stock = $product -> stock()-> create([
            'quantity' => 0,
            'reorder_quantity' => 100,
        ]);
    }
}
