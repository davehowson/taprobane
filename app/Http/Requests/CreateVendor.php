<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use App\Vendor;

class CreateVendor extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => 'required',
            'contact_number' => 'required',
            'credit_period' => 'required',
            'rating' => 'required',

            'billing-address-one'=>'required',
            'billing-address-two'=>'required',
            'billing-city' => 'required',
            'billing-postal-code' => 'required',
            'billing-country' => 'required',
        ];
    }

     public function persist(){
        $vendor = Vendor::create([
            'name' => request('name'),
            'contact_number' => request('contact_number'),
            'credit_period' => request('credit_period'),
            'rating' => request('rating'),
        ]);

        $vendor->addresses()->create([
            'address_one'=>request('billing-address-one'),
            'address_two'=>request('billing-address-two'),
            'city'=>request('billing-city'),
            'state'=>request('billing-state'),
            'postal_code'=>request('billing-postal-code'),
            'country'=>request('billing-country'),
            'isBilling'=>'1',
            'isDelivery'=>request('isDelivery'),
            'isDefault'=>request('isDefault-billing'),

        ]);

        if (request('isDelivery') == false) {
            $vendor->addresses()->create([
            'address_one'=>request('delivery-address-one'),
            'address_two'=>request('delivery-address-two'),
            'city'=>request('delivery-city'),
            'state'=>request('delivery-state'),
            'postal_code'=>request('delivery-postal-code'),
            'country'=>request('delivery-country'),
            'isBilling'=>'0',
            'isDelivery'=>'1',
            'isDefault'=>request('isDefault-delivery'),
            ]);
        }

        foreach ($this->product as $key => $value) {
            $product_id = $this->product[$key];
            $product = \App\Product::findOrFail($product_id);

            $product->vendors()->save($vendor);
        }

    }
}
