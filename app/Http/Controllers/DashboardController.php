<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
use DB;
use JavaScript;
use View;

class DashboardController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $countries = DB::table('customer_addresses')
                                ->select('country', DB::raw('count(*) as total'))
                                ->groupBy('country')
                                ->get();

        $countriesJSON = $countries->toJson();

        $customerCount = DB::table('customers')->count();

        $vendorCount = DB::table('vendors')->count();

        $productCount = DB::table('products')->count();

        JavaScript::put([
            'countries' => $countriesJSON,
            'customers' => $customerCount,
            'vendors' => $vendorCount,
            'products' => $productCount,
        ]);

        return View::make('dashboard.index');
    }
}
