<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Product;
use App\Category;
use App\Http\Requests\CreateProduct;
use App\Http\Requests\UpdateProduct;
use Illuminate\Support\Facades\Storage;

class ProductsController extends Controller
{
    public function __construct(){
        $this->middleware('auth');
    }
    
    public function index(){
    	 $products = Product::search()->orderBy('product_name')->paginate(20);
    	return view('products.index',compact('products'));
    }

    public function create(Request $request){
        $request->user()->authorizeRoles('Administrator');

        $categories = Category::orderBy('category_name')->get();

        return view('products.create',compact('categories'));
    }

    public function register(CreateProduct $form){

        $form->persist();

        session()->flash('message','Product Created');

        return redirect("/products");
    }

    public function show(Product $product){
        return view('products.show',compact('product'));
    }

    public function edit(Product $product, Request $request){
        $request->user()->authorizeRoles('Administrator');

        $categories = Category::orderBy('category_name')->get();

        return view('products.edit',compact('product','categories'));
    }

    public function update(Updateproduct $request){
        $request->user()->authorizeRoles('Administrator');
        $product = Product::findOrFail(request('product'));

        if (!($request->input('product_name')=='')){
            $product->product_name = $request->input('product_name');
        }

        if (!($request->input('product_description')=='')){
            $product->product_description = $request->input('product_description');
        }

        if (!($request->input('product_price')=='')){
            $product->product_price = $request->input('product_price');
        }

        if ($request->hasFile('product_image')){
            $path = Storage::putFile('public/products', request('product_image'));

            $path = explode('/',$path);

            $path = $path[2];

            Storage::delete('public/products/'.$product->product_image);

            $product->product_image = $path;

        }

        if (!($request->input('product_category')=='')){
            $product->category_id = $request->input('product_category');
        }


        $product->save();

        session()->flash('message','Product Updated');

        return redirect("/products");
    }

    public function delete(Product $product, Request $request){
        $request->user()->authorizeRoles('Administrator');

        if (!($product->product_image)=="product-image-placeholder.png"){
            Storage::delete('public/products/'.$product->product_image);
        }
       

        $product->delete();

        session()->flash('message','Product Deleted');

        return redirect("/products");
    }

}
