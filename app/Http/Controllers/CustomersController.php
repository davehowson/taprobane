<?php

namespace App\Http\Controllers;

use App\Customer;
use App\Http\Requests\UpdateCustomer;
use App\Http\Requests\CreateCustomer;
use Illuminate\Http\Request;

class CustomersController extends Controller
{
    public function __construct(){
    	$this->middleware('auth');
    }

    public function index(){
    	$customers = Customer::search()->orderBy('name')->paginate(20);
        return view('customers.index',compact('customers'));
    }

    public function create(Request $request){
        $request->user()->authorizeRoles('Administrator');
        return view('customers.create');
    }

    public function register(CreateCustomer $form){
        $form->persist();

        session()->flash('message','Customer Created');

        return redirect("/customers");
    }

    public function show(Customer $customer){
        return view('customers.show',compact('customer'));
    }

    public function edit(Customer $customer, Request $request){
        //IMPORTANT - THIS IS AN ADMIN FUNCTION AND SHOULD NOT BE USED BY CUSTOMERS

        //Dynamically displaying addresses

        //Billing 
        $billingAddresses = 0;

        $billingCount = 0;
        $billingCount = $customer->addresses()->where('isBilling','1')->count();
            if (($billingCount > 0)) {
                $billingAddresses = $customer->addresses()->where('isBilling','1') -> get();
                 
            }

        //Delivery
        $deliveryAddresses = 0;

        $deliveryCount = 0;
        $deliveryCount = $customer->addresses()->where('isDelivery','1')->count();
            if (($deliveryCount > 0)) {
                $deliveryAddresses = $customer->addresses()->where('isDelivery','1') -> get();  
            }

        $request->user()->authorizeRoles('Administrator');
        return view('customers.edit',compact('customer','deliveryAddresses','billingAddresses'));
    }

    public function update(UpdateCustomer $form){
        $form -> persist();

        session()->flash('message','Customer Updated');

        return redirect("/customers");
    }

    public function delete(Customer $customer, Request $request){
        $request->user()->authorizeRoles('Administrator');
        $customer->delete();

        session()->flash('message','Customer Deleted');

        return redirect("/customers");
    }
}
