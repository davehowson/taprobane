<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Vendor;
use App\Product;
use App\Http\Requests\CreateVendor;
use App\Http\Requests\UpdateVendor;

class VendorsController extends Controller
{
     public function __construct(){
    	$this->middleware('auth');
    }

    public function index(){
    	$vendors = Vendor::search()->orderBy('name')->paginate(20);
        return view('vendors.index',compact('vendors'));
    }

    public function create(Request $request){
        $request->user()->authorizeRoles('Administrator');

        $products = Product::orderBy('product_name')->get();
        return view('vendors.create',compact('products'));
    }

    public function register(CreateVendor $createVendor){
        $createVendor->persist();

        session()->flash('message','Vendor Created');

        return redirect("/vendors");
    }

    public function show(Vendor $vendor){
        return view('vendors.show',compact('vendor'));
    }

    public function edit(Vendor $vendor, Request $request){
        $request->user()->authorizeRoles('Administrator');


        $billingAddresses = null;

        $billingCount = 0;
        $billingCount = $vendor->addresses()->where('isBilling','1')->count();
            if (($billingCount > 0)) {
                $billingAddresses = $vendor->addresses()->where('isBilling','1') -> get();
                 
            }

        //Delivery
        $deliveryAddresses = null;

        $deliveryCount = 0;
        $deliveryCount = $vendor->addresses()->where('isDelivery','1')->count();
            if (($deliveryCount > 0)) {
                $deliveryAddresses = $vendor->addresses()->where('isDelivery','1') -> get();  
            }


        return view('vendors.edit',compact('vendor','deliveryAddresses','billingAddresses'));
    }

    public function update(UpdateVendor $request){
        $request->persist();

        session()->flash('message','Vendor Updated');

        return redirect("/vendors");
    }

    public function delete(Vendor $vendor, Request $request){
        $request->user()->authorizeRoles('Administrator');
        $vendor->delete();

        session()->flash('message','Vendor Deleted');

        return redirect("/vendors");
    }
}
