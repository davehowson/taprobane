<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Auth;
use App\Product;
use App\Invoice;
use App\InvoiceOrderLine;
use App\Delivery;
use App\Stock;
use App\StockLock;
use App\Http\Requests\InvoiceRequest;

class InvoiceController extends Controller
{
    public function __construct(){
    	$this->middleware('auth:customers');
    }

    public function create(){
        $products = Product::orderBy('product_name')->get();
        return view('invoices.invoice',compact('products'));
    }

    public function submit(InvoiceRequest $invoiceRequest){

        $total = 0;
        $productslist = [];

        $user = Auth::id();

        $invoice = Invoice::create(['customer_id' => $user,
									'payment_status' => 'unpaid',
									'cost' => 0,
									'delivery_id' => 0,
									'total_cost' => $total,
                                    ]);

        $invoiceId = $invoice->id;

        foreach ($invoiceRequest->product as $key => $value) {
        	$product_id = $invoiceRequest->product[$key];
        	$product = Product::findOrFail($product_id);
            $price = $product->product_price;

            $invoiceOrder = InvoiceOrderLine::create(['invoice_id' => $invoiceId,
           												'product_id' => $product_id,
														'unit_price' => $price,
														'quantity' => $invoiceRequest->quantity[$key],
        									]);
            $quantity = $invoiceRequest->quantity[$key];
            $cost = $price * $quantity;
            $total = $total + $cost;

           //stock locking
           $stockLock = $invoice->stockLock()->create([
                                            'customer_id' => $user,
                                            'product_id' => $product_id,
                                            'quantity' => $invoiceRequest->quantity[$key],
                                            ]);

           $stockBalance = $product->stock->quantity;
           $stockBalance = $stockBalance - $quantity;
           $product->stock->update(['quantity' => $stockBalance]);

           $product->stock->stockDispatchLog()->create(['type' => 'invoice',
                                                        'quantity' => $quantity,
                                                        'price' => $price,
                                                    ]);

           array_push($productslist,$product->product_name);
        }
        //initiate delivery
        $delivery = $invoice->delivery()->create([
        								'address_id' => $invoiceRequest->get('delivery-address'),
        								'delivery_status' => 'undelivered',
        							]);

        $deliveryLog = $delivery->deliverylog()->create([
        												'delivery_status' => 'undelivered'
        												]);


        $invoice->update(['delivery_id' => $delivery->id,
        					'total_cost' => $total]);



        return redirect()->route('cart', ['invoice' => $invoiceId]);
    }

    public function showCart(Invoice $invoice){

        $lines = $invoice -> invoiceOrderLines;

        return view('invoices.cart',compact('invoice','lines'));
    }

    public function cancel(Invoice $invoice){

        $lines = $invoice -> invoiceOrderLines;

        $delivery = $invoice -> delivery() -> first() ->deliverylog() ->create([
                                                                    'delivery_status' => 'cancelled'
                                                                    ]);

        foreach ($lines as $line) {
            $productId = $line->product_id;
            $quantity = $line->quantity;

            $currentQty = $line->product->stock->quantity;

            $updatedQty = $currentQty + $quantity;

            $stock = $line->product->stock->update(['quantity' => $updatedQty]);

            $line->product->stock->stockDispatchLog()->delete();

        }

        $invoice->delete();

        return redirect()->route('invoice');
    }
}
