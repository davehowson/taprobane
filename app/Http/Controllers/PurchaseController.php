<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Product;
use App\Purchase;
use App\Vendor;
use App\PurchaseOrderLine;
use App\Http\Requests\PurchaseRequest;

class PurchaseController extends Controller
{
    public function __construct(){
        $this->middleware('auth');
    }

    public function create(){
        $vendors = Vendor::orderBy('name')->get();
        return view('purchases.purchase',compact('vendors'));
    }

    public function submit(PurchaseRequest $purchaseRequest){


        $total = 0;

        $purchase = Purchase::create(['total_cost' => '0',
                                        'payment_status' => 'unpaid',
                                        'date' => $purchaseRequest-> input('date'),
                                    ]);

        $purchaseId = $purchase->id; 

        foreach ($purchaseRequest->product as $key => $value) {
        	$product_id = $purchaseRequest->product[$key];
        	$product = Product::findOrFail($product_id);
        	$price = $product->product_price;

           $purchaseOrder = PurchaseOrderLine::create(['product_id' => $product_id,
                                                'purchase_id' => $purchaseId,
                                                'vendor_id' => $purchaseRequest->vendor[$key],
                                                'unit_price' => $price,
                                                'quantity' => $purchaseRequest->quantity[$key],
        									]);
           $quantity = $purchaseRequest->quantity[$key];
           $cost = $price*$quantity;
           $total = $total + $cost;

           $stockBalance = $product->stock->quantity;
           $stockBalance = $stockBalance + $quantity;
           $product->stock->update(['quantity' => $stockBalance]);

           $product->stock->stockDispatchLog()->create(['type' => 'purchase',
                                                        'quantity' => $quantity,
                                                        'price' => $price,
                                                    ]);
        }

        $purchase->update(['total_cost' => $total]);

        return redirect()->route('purchaseReceipt', ['purchase' => $purchaseId]);
    }

     public function purchaseReceipt(Purchase $purchase){

        $lines = $purchase -> purchaseOrderLines;

        return view('purchases.receipt',compact('purchase','lines'));
    }
}
