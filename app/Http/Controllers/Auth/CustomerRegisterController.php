<?php

namespace App\Http\Controllers\Auth;

use Auth;
use App\Customer;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;
use Illuminate\Foundation\Auth\RegistersUsers;
use App\Transformers\CustomerTransformer;

use Illuminate\Auth\Events\Registered;

class CustomerRegisterController extends Controller
{
	/*
    |--------------------------------------------------------------------------
    | Register Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the registration of new customers as well as their
    | validation and creation. By default this controller uses a trait to
    | provide this functionality without requiring any additional code.
    |
    */
	use RegistersUsers;

	 /**
     * Where to redirect customers after registration.
     *
     * @var string
     */
	protected $redirectTo = '/invoice';

	/**
     * Create a new controller instance.
     *
     * @return void
     */
	public function __construct()
    {
        $this->middleware('guest:customers');
    }

    /**
     * Show the application registration form.
     *
     * @return \Illuminate\Http\Response
     */
    public function showRegistrationForm()
    {
        return view('auth.customer-register');
    }

    public function register(Request $request)
    {
        $this->validator($request->all())->validate();

        event(new Registered($user = $this->create($request->all())));

        $this->guard()->login($user);

        return redirect($this->redirectPath());


        // FOR API
        // return fractal()
        //     ->item($user)
        //     ->transformWith(new CustomerTransformer)
        //     ->toArray();
    }

     /**
     * Get a validator for an incoming registration request.
     *
     * @param  array  $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
     protected function validator(array $data)
    {
        return Validator::make($data, [
            'name' => 'required|string|max:255',
            'email' => 'required|string|email|max:255|unique:customers',
            'password' => 'required|string|min:8|confirmed',

            'billing-address-one'=>'required',
            'billing-address-two'=>'required',
            'billing-city' => 'required',
            'billing-postal-code' => 'required',
            'billing-country' => 'required',
        ]);
    }

    /**
     * Create a new user instance after a valid registration.
     *
     * @param  array  $data
     * @return \App\User
     */
    protected function create(array $data)
    {
    	$customer = Customer::create([
    		'name' => $data['name'],
    		'email' => $data['email'],
    		'password' => bcrypt($data['password']),
    	]);

        $customer->addresses()->create([
            'address_one'=>request('billing-address-one'),
            'address_two'=>request('billing-address-two'),
            'city'=>request('billing-city'),
            'state'=>request('billing-state'),
            'postal_code'=>request('billing-postal-code'),
            'country'=>request('billing-country'),
            'isBilling'=>'1',
            'isDelivery'=>request('isDelivery'),
            'isDefault'=>request('isDefault-billing'),

        ]);

        if (request('isDelivery') == false) {
            $customer->addresses()->create([
            'address_one'=>request('delivery-address-one'),
            'address_two'=>request('delivery-address-two'),
            'city'=>request('delivery-city'),
            'state'=>request('delivery-state'),
            'postal_code'=>request('delivery-postal-code'),
            'country'=>request('delivery-country'),
            'isBilling'=>'0',
            'isDelivery'=>'1',
            'isDefault'=>request('isDefault-delivery'),
            ]);
        }


        return $customer;
    }

    /**
     * Get the guard to be used during registration.
     *
     * @return \Illuminate\Contracts\Auth\StatefulGuard
     */
    protected function guard()
    {
    	return Auth::guard('customers');
    }
}
