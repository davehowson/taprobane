<?php

namespace App\Http\Controllers\Auth;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Auth;

class CustomerLoginController extends Controller
{
	public function __construct(){
		$this->middleware('guest:customers');
	}

    public function showLoginForm(){
    	return view('auth.customer-login');
    }

    public function login(Request $request){
    	//validate form data
    	$this->validate($request, [
    		'email' => 'required|email',
    		'password' => 'required'
    	]);

    	//attempt to log the customer in
    	if (Auth::guard('customers')->attempt(['email' => $request->email, 'password' => $request->password], $request->remember)){
    		return redirect()->route('invoice');
    	}

    	return redirect()->back()->withInput($request->only('email','remember'));
    }
}
