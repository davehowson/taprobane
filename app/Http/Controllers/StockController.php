<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\StockDispatchLog;

class StockController extends Controller
{
    public function __construct(){
        $this->middleware('auth');
    }

    public function logs(){
        $logs = StockDispatchLog::orderBy('created_at')->get();
        return view('stocks.log',compact('logs'));
    }
}
