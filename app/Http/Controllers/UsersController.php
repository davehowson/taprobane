<?php

namespace App\Http\Controllers;

//use Request;
use App\User;
use App\Role;
use Illuminate\Http\Request;
use App\Http\Requests\UpdateUser;
use App\Http\Requests\CreateUser;

class UsersController extends Controller
{
	public function __construct(){
    	$this->middleware('auth');
	}

    public function index(){
        $users = User::search()->orderBy('name')->paginate(20);
        return view('users.index',compact('users'));
    }

    public function create(Request $request){
        $request->user()->authorizeRoles('Administrator');
        return view('users.create');
    }

    public function register(CreateUser $form){
        $form->persist();

        session()->flash('message','User Created');

        return redirect("/users");
    }

    public function show(User $user){
        return view('users.show',compact('user'));
    }

    public function edit(User $user, Request $request){
        $request->user()->authorizeRoles('Administrator');
    	return view('users.edit',compact('user'));
    }

    public function update(UpdateUser $request){
        $request->user()->authorizeRoles('Administrator');
    	$user = User::findOrFail(request('user'));

    	if (!($request->input('name')=='')){
    		$user->name = $request->input('name');
    	}

    	if (!($request->input('email')=='')){
    		$user->email = $request->input('email');
    	}

    	if (!($request->input('password')=='')){
    		$user->password = bcrypt($request->input('password'));
    	}

        if (!($request->input('role')=='')){
           $user
            ->roles()
            ->attach(Role::where('id',request('role'))->first());
        }

    	$user->save();

    	session()->flash('message','User Updated');

    	return redirect("/users");
    }

    public function delete(User $user, Request $request){
        $request->user()->authorizeRoles('Administrator');
        $user->delete();

        session()->flash('message','User Deleted');

        return redirect("/users");
    }
}
