<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Category;
use App\Http\Requests\CreateCategory;
use App\Http\Requests\UpdateCategory;

class CategoriesController extends Controller
{
    public function __construct(){
        $this->middleware('auth');
    }
    
    public function index(){
    	 $categories = Category::search()->orderBy('category_name')->paginate(20);
    	return view('products.categories.index',compact('categories'));
    }

    public function create(Request $request){
        $request->user()->authorizeRoles('Administrator');

        return view('products.categories.create',compact('categories'));
    }

    public function register(CreateCategory $form){

        $form->persist();

        session()->flash('message','Category Created');

        return redirect("/categories");
    }

    public function show(Category $category){
        return view('products.categories.show',compact('category'));
    }

    public function edit(Category $category, Request $request){
        $request->user()->authorizeRoles('Administrator');

        return view('products.categories.edit',compact('category'));
    }

    public function update(UpdateCategory $request){
        $request->user()->authorizeRoles('Administrator');
        $category = Category::findOrFail(request('category'));

        if (!($request->input('category_name')=='')){
            $category->category_name = $request->input('category_name');
        }

        if (!($request->input('category_description')=='')){
            $category->category_description = $request->input('category_description');
        }

        $category->save();

        session()->flash('message','Category Updated');

        return redirect("/categories");
    }

    public function delete(Category $category, Request $request){
        $request->user()->authorizeRoles('Administrator');

        $category->delete();

        session()->flash('message','Category Deleted');

        return redirect("/Categories");
    }
}
