<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Vendor extends Model
{
    protected $fillable = [
        'name', 'contact_number', 'credit_period', 'rating',
    ];

    public function scopeSearch($query){
        return $query->where('name','like','%'.request()->search.'%');
    }

     public function addresses(){
         return $this->hasMany('App\VendorAddress');
    }

    public function purchaseOrderLines(){
    	return $this->hasMany('App\PurchaseOrderLines');
    }

    public function products()
    {
        return $this->belongsToMany('App\Product');
    }
}
