<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class StockDispatchLog extends Model
{
	protected $fillable = [
        'type', 'quantity', 'price',
    ];

    public function stock(){
    	return $this->belongsTo('App\Stock');
    }
}
