<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Stock extends Model
{
	protected $fillable = [
        'quantity',
    ];

    public function product()
    {
    	return $this->belongsTo('App\Product','id');
    }

    public function stockDispatchLog()
    {
        return $this->hasMany('App\StockDispatchLog');
    }
}
