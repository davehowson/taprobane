<?php

/**
* Used to transform JSON outputs using Fractal
*/

namespace App\Transformers;

use App\Customer;
use \League\Fractal\TransformerAbstract;

class CustomerTransformer extends TransformerAbstract
{
	
	public function transform(Customer $customer)
	{
		return [
			'username' => $customer->name,
		];
	}
}