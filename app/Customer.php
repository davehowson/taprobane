<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;

use Spatie\Activitylog\Traits\LogsActivity;
use App\Notifications\CustomerResetPasswordNotification;

use Laravel\Passport\HasApiTokens;

class Customer extends Authenticatable
{

	use Notifiable, HasApiTokens;

	protected $guard = 'customers';        //used for api authentication

	protected $fillable = [
        'name', 'email', 'password',
    ];

    protected $hidden = [
        'password', 'remember_token',
    ];


    public function scopeSearch($query){
        return $query->where('name','like','%'.request()->search.'%');
    }

    public function addresses(){
         return $this->hasMany('App\CustomerAddress');
    }

     public function customer(){
        return $this->hasMany('App\Invoice');
    }

    public function sendPasswordResetNotification($token)
    {
        $this->notify(new CustomerResetPasswordNotification($token));
    }
}
