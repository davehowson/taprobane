<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class StockLock extends Model
{
    protected $fillable = [
        'product_id', 'quantity',
    ];

     public function invoice()
    {
        return $this->belongsTo('App\Invoice');
    }
}
