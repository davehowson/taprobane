<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class VendorAddress extends Model
{
	protected $fillable = [
        'address_one', 'address_two', 'city', 'state', 'postal_code', 'country', 'isBilling', 'isDelivery'
    ];
    
    public function customer()
    {
        return $this->belongsTo('App\Vendor');
    }
}
