<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class InvoiceOrderLine extends Model
{
    protected $fillable = [
        'invoice_id', 'product_id', 'unit_price', 'quantity',
    ];


    public function invoice(){
    	return $this->belongsTo('App\Invoice');
    }

    public function product(){
    	return $this->belongsTo('App\Product');
    }
}
