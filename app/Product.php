<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Product extends Model
{
	protected $fillable = [
        'product_name', 'product_description', 'product_price', 'product_image', 'category_id',
    ];


    public function category()
    {
        return $this->belongsTo('App\Category');
    }

    public function stock()
    {
        return $this->hasOne('App\Stock','id');
    }

    public function vendors()
    {
        return $this->belongsToMany('App\Vendor');
    }

    public function invoiceOrderLines()
    {
        return $this->hasMany('App\InvoiceOrderLine');
    }

    public function purchaseOrderLines()
    {
        return $this->hasMany('App\PurchaseOrderLine');
    }

    public function scopeSearch($query){
        return $query->where('product_name','like','%'.request()->search.'%');
        
    }
}
