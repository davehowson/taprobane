<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Spatie\Activitylog\Traits\LogsActivity;

use Laravel\Passport\HasApiTokens;


class User extends Authenticatable
{
    use Notifiable, LogsActivity, HasApiTokens;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password',
    ];

    // The following are used for logging purposes

            public function getDescriptionForEvent(string $eventName): string
            {
                return "This user has been {$eventName}";
            }

            public function getLogNameToUse(string $eventName): string
            {
                return $eventName;
            }

            protected static $logAttributes = ['name', 'email'];

    //End of logging


    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    public function scopeSearch($query){
        return $query->where('name','like','%'.request()->search.'%');
        //not sure is this is ok when no parameters are passed
    }

    //The following are used to define Relationships

        public function roles(){
            return $this
                ->belongsToMany('App\Role')
                ->withTimestamps();
        }

    //End of Relationships

    public function authorizeRoles($roles){
        if ($this->hasAnyRole($roles)){
            return true;
        }
        abort(401,'This action is unauthorized');
    }

    public function hasAnyRole($roles){
        if (is_array($roles)){
            foreach ($roles as $role){
                if ($this->hasRole($role)){
                    return true;
                }
            }
        } else {
            if ($this->hasRole($roles)){
                return true;
            }
        }
        return false;
    }

    public function hasRole($role){
        if ($this->roles()->where('name',$role)->first()){
            return true;
        }
        return false;
    }
}
