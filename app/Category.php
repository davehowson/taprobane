<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Category extends Model
{
	protected $fillable = [
        'category_name', 'category_description',
    ];

    public function scopeSearch($query){
        return $query->where('category_name','like','%'.request()->search.'%');
    }

    public function products()
    {
        return $this->hasMany('App\Product');
    }
}
