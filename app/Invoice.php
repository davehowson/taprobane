<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Invoice extends Model
{
	protected $fillable = [
        'customer_id', 'payment_status', 'cost', 'delivery_id', 'total_cost',
    ];

	public function invoiceOrderLines(){
		return $this->hasMany('App\InvoiceOrderLine');
	}

	public function delivery(){
		return $this->hasOne('App\Delivery');
	}

    public function customer(){
    	return $this->belongsTo('App\Customer');
    }

    public function stockLock()
    {
        return $this->hasMany('App\StockLock');
    }
}
