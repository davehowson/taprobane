<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class DeliveryLog extends Model
{
	protected $fillable = [
        'delivery_status',
    ];

    public function delivery(){
    	$this->belongsTo('App\Delivery');
    }
}
