<?php

use Illuminate\Database\Seeder;

class VendorAddressTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
         $vendorAddresses = factory(App\VendorAddress::class, 10)->create();
    }
}
