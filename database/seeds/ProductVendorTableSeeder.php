<?php

use Illuminate\Database\Seeder;

class ProductVendorTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
    	$vendors = App\Vendor::all();

        App\Product::all()->each(function($product) use ($vendors){
        	$product->vendors()->attach(
        		$vendors->random(rand(1,3))->pluck('id')->toArray()
        	);
        });
    }
}
