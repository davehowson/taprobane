<?php

use Illuminate\Database\Seeder;
use App\User;
use App\Role;

class UserTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $role_admin = Role::where('name', 'Administrator')->first();
    	$role_moderator  = Role::where('name', 'Moderator')->first();

    	$admin = new User();
    	$admin->name = 'John Doe';
    	$admin->email = 'john@example.com';
    	$admin->password = bcrypt('password');
    	$admin->save();
    	$admin->roles()->attach($role_admin);

    	$moderator = new User();
    	$moderator->name = 'Peter Griffin';
    	$moderator->email = 'peter@example.com';
    	$moderator->password = bcrypt('password');
    	$moderator->save();
    	$moderator->roles()->attach($role_moderator);

        factory(App\User::class, 10)->create();
    }
}
