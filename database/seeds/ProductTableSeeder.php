<?php

use Illuminate\Database\Seeder;
use App\Product;

class ProductTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // $products = factory(App\Product::class, 10)->create();

        //   factory(App\Product::class, 10)->create()->each(function ($u) {
        //    	$u->stock()->save(factory(App\Stock::class)->make());
    	// });

        Product::create([
            'product_name' => 'Marie Biscuit',
            'product_description' => 'A packet of Marie Biscuits',
            'product_image' => 'product-image-placeholder.png',
            'product_price' => '100',
            'category_id' => '1',
        ])->stock()->save(factory(App\Stock::class)->make());

        Product::create([
            'product_name' => 'Detol Soap',
            'product_description' => 'A soap',
            'product_image' => 'product-image-placeholder.png',
            'product_price' => '50',
            'category_id' => '2',
        ])->stock()->save(factory(App\Stock::class)->make());

        Product::create([
            'product_name' => 'Sunsilk Shampoo',
            'product_description' => 'A shampoo',
            'product_image' => 'product-image-placeholder.png',
            'product_price' => '75',
            'category_id' => '2',
        ])->stock()->save(factory(App\Stock::class)->make());

        Product::create([
            'product_name' => 'Casio Watch',
            'product_description' => 'A watch',
            'product_image' => 'product-image-placeholder.png',
            'product_price' => '1000',
            'category_id' => '3',
        ])->stock()->save(factory(App\Stock::class)->make());

        Product::create([
            'product_name' => 'Lemon Puff',
            'product_description' => 'A packet of Lemon Puff',
            'product_image' => 'product-image-placeholder.png',
            'product_price' => '100',
            'category_id' => '1',
        ])->stock()->save(factory(App\Stock::class)->make());

        Product::create([
            'product_name' => 'Ginger Beer',
            'product_description' => '200ml Bottle',
            'product_image' => 'product-image-placeholder.png',
            'product_price' => '200',
            'category_id' => '1',
        ])->stock()->save(factory(App\Stock::class)->make());

        Product::create([
            'product_name' => 'Calculator',
            'product_description' => 'Standard Calculator',
            'product_image' => 'product-image-placeholder.png',
            'product_price' => '600',
            'category_id' => '3',
        ])->stock()->save(factory(App\Stock::class)->make());

        Product::create([
            'product_name' => 'Laptop',
            'product_description' => 'An i5 Laptop',
            'product_image' => 'product-image-placeholder.png',
            'product_price' => '10000',
            'category_id' => '3',
        ])->stock()->save(factory(App\Stock::class)->make());

        Product::create([
            'product_name' => 'Maggie Noodles',
            'product_description' => 'A packet of Maggie Noodles',
            'product_image' => 'product-image-placeholder.png',
            'product_price' => '60',
            'category_id' => '1',
        ])->stock()->save(factory(App\Stock::class)->make());

        Product::create([
            'product_name' => 'Ice Cream',
            'product_description' => '200ml Chocolate',
            'product_image' => 'product-image-placeholder.png',
            'product_price' => '200',
            'category_id' => '1',
        ])->stock()->save(factory(App\Stock::class)->make());

        Product::create([
            'product_name' => 'Noodles',
            'product_description' => 'Noodles Packet',
            'product_image' => 'product-image-placeholder.png',
            'product_price' => '50',
            'category_id' => '1',
        ])->stock()->save(factory(App\Stock::class)->make());

        Product::create([
            'product_name' => 'Speakers',
            'product_description' => 'Creative 2.0',
            'product_image' => 'product-image-placeholder.png',
            'product_price' => '2000',
            'category_id' => '3',
        ])->stock()->save(factory(App\Stock::class)->make());

        Product::create([
            'product_name' => 'USB Memory Stick',
            'product_description' => '2GB',
            'product_image' => 'product-image-placeholder.png',
            'product_price' => '1000',
            'category_id' => '3',
        ])->stock()->save(factory(App\Stock::class)->make());

        Product::create([
            'product_name' => 'Keyboard',
            'product_description' => 'US',
            'product_image' => 'product-image-placeholder.png',
            'product_price' => '100',
            'category_id' => '3',
        ])->stock()->save(factory(App\Stock::class)->make());

        Product::create([
            'product_name' => 'Mouse',
            'product_description' => 'USB Mouse',
            'product_image' => 'product-image-placeholder.png',
            'product_price' => '120',
            'category_id' => '3',
        ])->stock()->save(factory(App\Stock::class)->make());

        Product::create([
            'product_name' => 'Monito',
            'product_description' => 'LCD',
            'product_image' => 'product-image-placeholder.png',
            'product_price' => '1000',
            'category_id' => '3',
        ])->stock()->save(factory(App\Stock::class)->make());
    }
}
