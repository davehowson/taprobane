<?php

use Illuminate\Database\Seeder;
use App\Category;

class CategoryTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // $category = factory(App\Category::class, 10)->create();
    	Category::create([
    		'category_name' => 'Food',
    		'category_description' => 'This contains edible items',
    	]);

    	Category::create([
    		'category_name' => 'Sanitary',
    		'category_description' => 'This contains Sanitary items',
    	]);

    	Category::create([
    		'category_name' => 'Electronics',
    		'category_description' => 'This contains Electronics',
    	]);
    }
}
