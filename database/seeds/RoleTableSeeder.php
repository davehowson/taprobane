<?php

use Illuminate\Database\Seeder;
use App\Role;

class RoleTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $role_admin = new Role();
    	$role_admin->name = 'Administrator';
    	$role_admin->description = 'An Admin User';
    	$role_admin->save();

    	$role_moderator = new Role();
    	$role_moderator->name = 'Moderator';
    	$role_moderator->description = 'A Moderator User';
    	$role_moderator->save();
    }
}
