<?php

use Illuminate\Database\Seeder;
use App\CustomerAddress;

class CustomerAddressTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $customerAddresses = factory(App\CustomerAddress::class, 100)->create();
    }
}
