<?php

use Faker\Generator as Faker;
use App\Customer;


$factory->define(App\CustomerAddress::class, function (Faker $faker) {
    static $password;

    return [
        'customer_id' => factory(Customer::class)->create()->id,
        'address_one' => $faker->streetAddress,
        'address_two' => $faker->streetName,
     	'city'=> $faker->city,
     	'state'=> $faker->state,
     	'postal_code'=> $faker->postcode,
     	'country'=> $faker->countryCode,
     	'isBilling'=> '1',
     	'isDelivery'=> '1'
    ];
});
