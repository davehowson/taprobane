<?php

use Faker\Generator as Faker;
use App\Vendor;


$factory->define(App\VendorAddress::class, function (Faker $faker) {
    static $password;

    return [
        'vendor_id' => factory(Vendor::class)->create()->id,
        'address_one' => $faker->streetAddress,
        'address_two' => $faker->streetName,
     	'city'=> $faker->city,
     	'state'=> $faker->state,
     	'postal_code'=> $faker->postcode,
     	'country'=> $faker->country,
     	'isBilling'=> '1',
     	'isDelivery'=> '1',

    ];
});
