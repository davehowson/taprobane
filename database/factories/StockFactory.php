<?php

use Faker\Generator as Faker;


$factory->define(App\Stock::class, function (Faker $faker) {
    return [
        'quantity' => "100",
        'reorder_quantity' => "100",
    ];
});
