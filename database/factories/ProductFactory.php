<?php

use Faker\Generator as Faker;
use App\Category;


$factory->define(App\Product::class, function (Faker $faker) {
$categories = App\Category::pluck('id')->toArray();
    return [
        'product_name' => $faker->word,
        'product_description' => $faker->sentence($nbWords = 6, $variableNbWords = true),
        'product_image' => "product-image-placeholder.png",
        'product_price' => $faker->numberBetween($min = 1000, $max = 9000),
        'category_id' => $faker->randomElement($categories),
    ];
});
