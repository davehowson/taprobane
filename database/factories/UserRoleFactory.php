<?php

use Faker\Generator as Faker;
use App\User;
use App\Role;


$factory->define(App\Role::class, function (Faker $faker) {
    static $password;

    return [
        'role_id' => factory(Role::class)->create()->id,
        'user_id' => factory(User::class)->create()->id,
    ];
});
