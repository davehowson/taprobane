<?php

use Faker\Generator as Faker;
use App\Category;


$factory->define(App\Category::class, function (Faker $faker) {

    return [
        'category_name' => $faker->word,
        'category_description' => $faker->sentence($nbWords = 6, $variableNbWords = true)
    ];
});
