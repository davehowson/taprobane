<?php

use Faker\Generator as Faker;

$factory->define(App\Vendor::class, function (Faker $faker) {
    return [
        'name' => $faker->name,
        'contact_number' => $faker->phoneNumber,
        'credit_period' => $faker->randomDigitNotNull,
        'rating' => $faker-> numberBetween($min = 0, $max = 10),
    ];
});
