<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});


Route::prefix('customer')->group(function(){
	Route::get('/register', 'Auth\CustomerRegisterController@showRegistrationForm')->name('customer.register');
	Route::post('/register', 'Auth\CustomerRegisterController@register');
	Route::get('/login', 'Auth\CustomerLoginController@showLoginForm')->name('customer.login');
	Route::post('/login', 'Auth\CustomerLoginController@login')->name('customer.login.submit');

	Route::post('logout', 'Auth\LoginController@logout')->name('logout');
});


