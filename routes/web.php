<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
//Login Routes
//Admins login using default guard
Auth::routes();

//disables the register route
Route::match(['get', 'post'], 'register', function(){
    return redirect('/');
});

//Customers login and register using customer guard
Route::prefix('customer')->group(function(){
	Route::get('/register', 'Auth\CustomerRegisterController@showRegistrationForm')->name('customer.register');
	Route::post('/register', 'Auth\CustomerRegisterController@register');
	Route::get('/login', 'Auth\CustomerLoginController@showLoginForm')->name('customer.login');
	Route::post('/login', 'Auth\CustomerLoginController@login')->name('customer.login.submit');

	//Password Reset Routes
	Route::post('/password/email', 'Auth\CustomerForgotPasswordController@sendResetLinkEmail')->name('customer.password.email');
	Route::get('/password/reset', 'Auth\CustomerForgotPasswordController@showLinkRequestForm')->name('customer.password.request');
	Route::post('/password/reset', 'Auth\CustomerResetPasswordController@reset');
	Route::get('/password/reset/{token}', 'Auth\CustomerResetPasswordController@showResetForm')->name('customer.password.reset');
});


//Homepage
Route::get('/', 'HomeController@index')->name('home');

Route::get('/dashboard', 'DashboardController@index')->name('dashboard');

// User Routes

Route::get('/users','UsersController@index');

Route::get('users/create', 'UsersController@create');
Route::post('users/create', 'UsersController@register');

Route::get('/users/{user}','UsersController@show');

Route::get('/users/edit/{user}','UsersController@edit');
Route::post('/users/edit/{user}','UsersController@update');

Route::get('/users/delete/{user}','UsersController@delete');

// Customer Routes

Route::get('/customers','CustomersController@index');

Route::get('customers/create', 'CustomersController@create');
Route::post('customers/create', 'CustomersController@register');

Route::get('/customers/{customer}','CustomersController@show');

Route::get('/customers/edit/{customer}','CustomersController@edit');
Route::post('/customers/edit/{customer}','CustomersController@update');

Route::get('/customers/delete/{customer}','CustomersController@delete');

// Vendor Routes

Route::get('/vendors','VendorsController@index');

Route::get('vendors/create', 'VendorsController@create');
Route::post('vendors/create', 'VendorsController@register');

Route::get('/vendors/{vendor}','VendorsController@show');

Route::get('/vendors/edit/{vendor}','VendorsController@edit');
Route::post('/vendors/edit/{vendor}','VendorsController@update');

Route::get('/vendors/delete/{vendor}','VendorsController@delete');

// Product Routes

Route::get('/products','ProductsController@index');

Route::get('products/create', 'ProductsController@create');
Route::post('products/create', 'ProductsController@register');

Route::get('/products/{product}','ProductsController@show');

Route::get('/products/edit/{product}','ProductsController@edit');
Route::post('/products/edit/{product}','ProductsController@update');

Route::get('/products/delete/{product}','ProductsController@delete');


//Category Routes

Route::get('/categories','CategoriesController@index');

Route::get('categories/create', 'CategoriesController@create');
Route::post('categories/create', 'CategoriesController@register');

Route::get('/categories/{category}','CategoriesController@show');

Route::get('/categories/edit/{category}','CategoriesController@edit');
Route::post('/categories/edit/{category}','CategoriesController@update');

Route::get('/categories/delete/{category}','CategoriesController@delete');

//Purchasing

Route::get('/purchase','PurchaseController@create');

Route::post('/purchase/submit', 'PurchaseController@submit');

Route::get('/purchase/{purchase}','PurchaseController@purchaseReceipt')->name('purchaseReceipt');

//Invoices

Route::get('/invoice','InvoiceController@create')->name('invoice');

Route::post('/invoice/submit', 'InvoiceController@submit');

Route::get('/cart/{invoice}','InvoiceController@showCart')->name('cart');

Route::get('/cart/{invoice}/cancel','InvoiceController@cancel');

//Stock Dispatch Log

Route::get('/stockdispatchlogs','StockController@logs');