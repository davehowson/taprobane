
/**
 * First we will load all of this project's JavaScript dependencies which
 * includes Vue and other libraries. It is a great starting point when
 * building robust, powerful web applications using Vue and Laravel.
 */

require('./bootstrap');		//this is the bootstrap.js file in the folder



/**
 * Next, we will create a fresh Vue application instance and attach it to
 * the page. Then, you may begin adding components to this application
 * or customize the JavaScript scaffolding to fit your unique needs.
 */

 
// import $ from 'jquery';
// window.$ = window.jQuery = $;

import 'jquery-ui/ui/widgets/datepicker.js';

import 'jquery-ui/ui/widgets/dialog.js';

$( function() {
    $( "#datepicker" ).datepicker({
    	minDate: 0,
    	dateFormat: "yy-mm-dd",
    });
} );

//icons
import fontawesome from '@fortawesome/fontawesome';
import faUser from '@fortawesome/fontawesome-free-solid/faUser';
import faBars from '@fortawesome/fontawesome-free-solid/faBars';
import faSearch from '@fortawesome/fontawesome-free-solid/faSearch';
import faTachometerAlt from '@fortawesome/fontawesome-free-solid/faTachometerAlt';
import faShoppingBag from '@fortawesome/fontawesome-free-solid/faShoppingBag';
import faBoxes from '@fortawesome/fontawesome-free-solid/faBoxes';
import faUsers from '@fortawesome/fontawesome-free-solid/faUsers';
import faHandshake from '@fortawesome/fontawesome-free-solid/faHandshake';
import faTruck from '@fortawesome/fontawesome-free-solid/faTruck';
import faCheck from '@fortawesome/fontawesome-free-solid/faCheck';
import faShoppingCart from '@fortawesome/fontawesome-free-solid/faShoppingCart';
import faBook from '@fortawesome/fontawesome-free-solid/faBook';
import faFileAlt from '@fortawesome/fontawesome-free-solid/faFileAlt';


fontawesome.library.add(faUser, faBars, faSearch, faTachometerAlt, faTachometerAlt, faShoppingBag, faBoxes, faUsers, faHandshake, faTruck, faCheck, faShoppingCart, faBook, faFileAlt);

import jVectorMaps from './jquery-jvectormap-2.0.3.min.js';
import worldMap from './jquery-jvectormap-world-mill.js';

import chartjs from 'chart.js';