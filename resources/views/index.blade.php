@extends('layouts.app')
@section('content')

	<div class="container" id="index">
		<div class="row">
			<div class="panel panel-login">
				<div class="row">
					<div class="col-md-6 col-md-offset-3" id="index-logo">
						<img src="{{URL::asset('images/logo.png')}}" class="img-responsive" width="500">
					</div>
				</div>
				<div class="row">
					<div id="index-text">
						This Admin-Dashboard was originally developed as part of an eCommerce project. But due to some management issues, it had to be abandoned amidst development. However, since a major portion of the dashboard had been completed, I decided to finish developing the admin portion and host it so that I can use it for my portfolio. I developed the UI using Bootstrap and coded the backend using Laravel. Most of the admin functionality is complete and with some minimal API configuration, this backend is ready to work with a mobile application (which was the original plan). You can login using the credentials below and try out the dashboard for yourself. Thank You.
					</div>
				</div>
				<div class="row">
					<ul id="index-list">
						<li>Email : john@example.com</li>
						<li>Password: password</li>
					</ul>
				</div>
				<div class="row">
					<div id="index-button"> 
						<a href="{{ url('/login') }}" class="btn btn-primary">Proceed to Login</a>
					</div>
				</div>
				
			</div>
			
		</div>
	</div>

@endsection