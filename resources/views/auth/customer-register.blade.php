@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default">
                <div class="panel-heading">Register Customer</div>

                <div class="panel-body">
                    <form class="form-horizontal" method="POST" action="{{ route('customer.register') }}">
                        {{ csrf_field() }}

                        <div class="form-group{{ $errors->has('name') ? ' has-error' : '' }}">
                            <label for="name" class="col-md-4 control-label">Name</label>

                            <div class="col-md-6">
                                <input id="name" type="text" class="form-control" name="name" value="{{ old('name') }}" required autofocus>

                                @if ($errors->has('name'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('name') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
                            <label for="email" class="col-md-4 control-label">E-Mail Address</label>

                            <div class="col-md-6">
                                <input id="email" type="email" class="form-control" name="email" value="{{ old('email') }}" required>

                                @if ($errors->has('email'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('email') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}">
                            <label for="password" class="col-md-4 control-label">Password</label>

                            <div class="col-md-6">
                                <input id="password" type="password" class="form-control" name="password" required>

                                @if ($errors->has('password'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('password') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group">
                            <label for="password-confirm" class="col-md-4 control-label">Confirm Password</label>

                            <div class="col-md-6">
                                <input id="password-confirm" type="password" class="form-control" name="password_confirmation" required>
                            </div>
                        </div>

                       {{-- ADDRESS --}}

                       <div class="form-group" id="billing-address">
                
                            <div class="form-group">
                                <div class="col-md-6 control-label">
                                    <h3>Billing Address</h3>
                                </div>
                            </div>


                            <div class="form-group">
                                <label for="billing-address-one" class="col-md-4 control-label">Address Line 1:</label>
                                <div class="col-md-6">
                                    <input type="text" class="form-control" id="billing-address-one" name="billing-address-one" required>
                                </div>
                            </div>

                            <div class="form-group">
                                <label for="billing-address-two" class="col-md-4 control-label">Address Line 2:</label>
                                <div class="col-md-6">
                                    <input type="text" class="form-control" id="billing-address-two" name="billing-address-two">
                                </div>
                            </div>

                            <div class="form-group">
                                <label for="billing-city" class="col-md-4 control-label">City:</label>
                                <div class="col-md-6">
                                    <input type="text" class="form-control" id="billing-city" name="billing-city" required>
                                </div>
                            </div>

                            <div class="form-group">
                                <label for="billing-state" class="col-md-4 control-label">State:</label>
                                <div class="col-md-6">
                                    <input type="text" class="form-control" id="billing-state" name="billing-state">
                                </div>
                            </div>

                            <div class="form-group">
                                <label for="billing-postal-code" class="col-md-4 control-label">Postal Code:</label>
                                <div class="col-md-6">
                                    <input type="text" class="form-control" id="billing-postal-code" name="billing-postal-code">
                                </div>
                            </div>

                            <div class="form-group">
                                <label for="billing-country" class="col-md-4 control-label">Country:</label>
                                <div class="col-md-6">
                                    <input type="text" class="form-control" id="billing-country" name="billing-country" required>
                                </div>
                            </div>

                            <div class="form-group centered-col">

                            <div class="checkbox col-sm-8">
                                <label style="font-size: 1.1em">
                                    <input type="checkbox" id="isDelivery" value="1" name="isDelivery" checked>
                                    <span class="cr"><i class="cr-icon fa fa-check"></i></span>
                                    Same Address for Delivery?
                                </label>
                            </div>
                        
                    </div>

                        </div>

                        <div class="form-group" id="delivery-address">
                            
                            <div class="form-group">
                                <div class="col-md-6 control-label">
                                    <h3>Delivery Address</h3>
                                </div>
                            </div>

                            <div class="form-group">
                                <label for="delivery-address-one" class="col-md-4 control-label">Address Line 1:</label>
                                <div class="col-md-6">
                                    <input type="text" class="form-control" id="delivery-address-one" name="delivery-address-one">
                                </div>
                            </div>

                            <div class="form-group">
                                <label for="delivery-address-two" class="col-md-4 control-label">Address Line 2:</label>
                                <div class="col-md-6">
                                    <input type="text" class="form-control" id="delivery-address-two" name="delivery-address-two">
                                </div>
                            </div>

                            <div class="form-group">
                                <label for="delivery-city" class="col-md-4 control-label">City:</label>
                                <div class="col-md-6">
                                    <input type="text" class="form-control" id="delivery-city" name="delivery-city" >
                                </div>
                            </div>

                            <div class="form-group">
                                <label for="delivery-state" class="col-md-4 control-label">State:</label>
                                <div class="col-md-6">
                                    <input type="text" class="form-control" id="delivery-state" name="delivery-state">
                                </div>
                            </div>

                            <div class="form-group">
                                <label for="delivery-postal-code" class="col-md-4 control-label">Postal Code:</label>
                                <div class="col-md-6">
                                    <input type="text" class="form-control" id="delivery-postal-code" name="delivery-postal-code">
                                </div>
                            </div>

                            <div class="form-group">
                                <label for="delivery-country" class="col-md-4 control-label">Country:</label>
                                <div class="col-md-6">
                                    <input type="text" class="form-control" id="delivery-country" name="delivery-country">
                                </div>
                            </div>

                        </div>


                        <div class="form-group">
                            <div class="col-md-6 col-md-offset-4">
                                <button type="submit" class="btn btn-primary">
                                    Register
                                </button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

@section('script') {{-- needs work --}}
    <script>
    $("#delivery-address").hide();
$("#isDelivery").click(function() {
    
    if($(this).is(":checked")) {
        $("#delivery-address").hide(200);
    } else {
        $("#delivery-address").show(300);
    }
});
    </script>
@endsection
