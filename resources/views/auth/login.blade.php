@extends('layouts.app')

@section('content')
<div id="login-bg">
    <div class="container">
            <div class="row">
                <div class="col-md-6 col-md-offset-3" id="login-form">
                    <div class="panel panel-login">
                        <div class="panel-body">
                            <div class="panel-heading">Login</div>
                            <div class="row">
                                <div class="col-lg-12">
                                    <form id="login-form" method="POST" role="form" action="{{ route('login') }}">
                                        {{ csrf_field() }}

                                        <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
                                            <input type="text" name="email" id="email" tabindex="1" class="form-control" placeholder="Email" name="email" value="{{ old('email') }}" required autofocus>

                                            @if ($errors->has('email'))
                                                <span class="help-block">
                                                    <strong>{{ $errors->first('email') }}</strong>
                                                </span>
                                            @endif
                                        </div>


                                        <div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}">
                                            <input type="password" name="password" id="password" tabindex="2" class="form-control" placeholder="Password" required>

                                            @if ($errors->has('password'))
                                                <span class="help-block">
                                                    <strong>{{ $errors->first('password') }}</strong>
                                                </span>
                                            @endif
                                        </div>


                                        <div class="checkbox form-group text-center" id="login-remember">
                                            <label style="font-size: 1.1em">
                                                <input type="checkbox" id="remember" value="1" name="remember" tabindex="3" {{ old('remember') ? 'checked' : '' }}>
                                                <span class="cr"><i class="cr-icon fa fa-check"></i></span>
                                                Remember Me
                                            </label>
                                        </div>


                                        <div class="form-group">
                                            <div class="row">
                                                <div class="col-sm-6 col-sm-offset-3">
                                                    <input type="submit" id="login-submit" tabindex="4" class="form-control btn btn-login" value="Log In">
                                                </div>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <div class="row">
                                                <div class="col-lg-12">
                                                    <div class="text-center">
                                                        <a href="{{ route('password.request') }}" tabindex="5" class="forgot-password">Forgot Password?</a>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
    </div>
</div>
@endsection
