@extends('layouts.master')

@section('title','Dashboard')

@section('content')


	<div class="row">
   <div class="col-lg-6">
     <div class="card" id="world-map-card">
       <div class="card-body">
          <h3>Customer-Base</h3>
           <div class="contaner">
               <div id="world-map"></div>
          </div>
       </div>
     </div>
   </div> 
   <div class="col-lg-6">
     <div class="card" id="stats-card1">
       <div class="card-body">
          <h3>Monthly Sales</h3>
         <canvas id="myChart"></canvas>
       </div>
     </div>
   </div>
  </div>



  <div class="row" id="stats">
    <div class="col-lg-4">
      <div class="card count" id="customer-count">
        <div class="card-body">
          <div class="count-info">
            <div class="count-header">
              <h3 id="customer-count-text"></h3>
            </div>
            <div class="count-text">
              <span>Customers</span>
            </div>
          </div>
        </div>
      </div>
    </div>

    <div class="col-lg-4">
      <div class="card count" id="vendor-count">
        <div class="card-body">
          <div class="count-info">
            <div class="count-header">
              <h3 id="vendor-count-text"></h3>
            </div>
            <div class="count-text">
              <span>Vendors</span>
            </div>
          </div>
        </div>
      </div>
    </div>

    <div class="col-lg-4">
      <div class="card count" id="product-count">
        <div class="card-body">
          <div class="count-info">
            <div class="count-header">
              <h3 id="product-count-text"></h3>
            </div>
            <div class="count-text">
              <span>Products</span>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>

@stop





@section('script')
  <script>

    $(function(){

      var countries = ServerVariables.countries;
      var countriesJSON = JSON.parse(countries);
      
      var mapData = {};

      $.each(countriesJSON, function(i, item){
        mapData[countriesJSON[i].country] = countriesJSON[i].total;
      });



      $('#world-map').vectorMap({
        map: 'world_mill',
        backgroundColor: '#FFFFFF',
        zoomButtons: false,
        series: {
          regions: [{
            values: mapData,
            scale: ['#C6E7FF', '#0074C6'],
            normalizeFunction: 'polynomial'
          }]
        },
        onRegionTipShow: function(e, el, code){
          el.html(el.html()+' (Users : '+mapData[code]+')');
        },
        regionStyle: {
            initial: {
            fill: '#C0C0C0FF',
            "fill-opacity": 1,
            stroke: 'none',
            "stroke-width": 0,
            "stroke-opacity": 1
          },
        } 
      });

      //bar chart
      var ctx = document.getElementById('myChart').getContext('2d');
      var chart = new Chart(ctx, {
        type: 'line',
        responsive: true,
        data: {
        labels: ["January", "February", "March", "April", "May", "June", "July"],
        datasets: [{
            label: "Sales",
            borderColor: '#F44336',
            data: [0, 10, 5, 2, 20, 30, 45],
          }]
        },
        options: {}
      });

      var customers = ServerVariables.customers;
      $('#customer-count-text').text(customers);

      var vendors = ServerVariables.vendors;
      $('#vendor-count-text').text(vendors);

      var products = ServerVariables.products;
      $('#product-count-text').text(products);
    });
  </script>
@endsection
