<!-- Sidebar Holder -->
            <nav id="sidebar">
                <div class="sidebar-header">
                    <img src="{{URL::asset('images/logo.png')}}" width="200">
                </div>

                <ul class="list-unstyled components">
                    <li class="{{ Request::is('dashboard') ? 'active' : '' }}">
                        <a href="{{ url('/dashboard') }}"><span class="sidebar-icon" id="fatachometer"><i class="fas fa-tachometer-alt" aria-hidden="true"></i></span>Dashboard</a>
                    </li>

                    <li class="{{ Request::is('products') ? 'active' : '' }}">
                    	<a href="{{ url('/products') }}"><span class="sidebar-icon" id="fashopping-bag"><i class="fas fa-shopping-bag" aria-hidden="true"></i></span>Products</a>
                    </li>

                    <li class="{{ Request::is('categories') ? 'active' : '' }}">
                    	<a href="{{ url('/categories') }}"><span class="sidebar-icon" id="faboxes"><i class="fas fa-boxes" aria-hidden="true"></i></span>Categories</a>
                    </li>

                    <li class="{{ Request::is('users') ? 'active' : '' }}">
                        <a href="{{ url('/users') }}"><span class="sidebar-icon" id="fausers"><i class="fas fa-users" aria-hidden="true"></i></span>Users</a>
                    </li>

                    <li class="{{ Request::is('customers') ? 'active' : '' }}">
                        <a href="{{ url('/customers') }}"><span class="sidebar-icon" id="facustomers"><i class="fas fa-handshake" aria-hidden="true"></i></span>Customers</a>
                    </li>

                    <li class="{{ Request::is('vendors') ? 'active' : '' }}">
                        <a href="{{ url('/vendors') }}"><span class="sidebar-icon" id="fatruck"><i class="fas fa-truck" aria-hidden="true"></i></span>Vendors</a>
                    </li>

                    
                    @if (Auth::guard('web')->check())
                        <li class="{{ Request::is('purchase') ? 'active' : '' }}">
                            <a href="{{ url('/purchase') }}"><span class="sidebar-icon" id="fashopping-cart"><i class="fas fa-shopping-cart" aria-hidden="true"></i></span>Purchase</a>
                        </li>
                    @elseif (Auth::guard('customers')->check())
                        <li class="{{ Request::is('invoice') ? 'active' : '' }}">
                            <a href="{{ url('/invoice') }}"><span class="sidebar-icon" id="fafile-alt"><i class="fas fa-file-alt" aria-hidden="true"></i></span>Invoice</a>
                        </li>
                    @endif

                    <li class="{{ Request::is('stockdispatchlogs') ? 'active' : '' }}">
                        <a href="{{ url('/stockdispatchlogs') }}"><span class="sidebar-icon" id="fabook"><i class="fas fa-book" aria-hidden="true"></i></span>Stock Logs</a>
                    </li>

                </ul>
            </nav>