<!-- Navigation Bar -->
                <nav role="navigation" class="navbar navbar-default navbar-fixed-top">
                    <div class="container-fluid">
                        <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                            <div class="navbar-header">
                                <button type="button" id="sidebarCollapse" class="btn btn-default navbar-btn">
                                    <i class="fas fa-bars" aria-hidden="true"></i>
                                </button>
                                <a class="navbar-brand" href="#">@yield('title')</a>
                            </div>

                            <ul class="nav navbar-nav navbar-right">
                                @guest
                                    <li><a href="{{ route('login') }}">Login</a></li>
                                @else
                                    <li class="dropdown">
                                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">
                                            <i class="fas fa-user" aria-hidden="true"></i>&nbsp;
                                            {{ Auth::user()->name }} <span class="caret"></span>
                                        </a>

                                        <ul class="dropdown-menu" role="menu">
                                            <li>
                                                <a href="{{ route('logout') }}"
                                                    onclick="event.preventDefault();
                                                             document.getElementById('logout-form').submit();">
                                                    Logout
                                                </a>

                                                <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                                    {{ csrf_field() }}
                                                </form>
                                            </li>
                                        </ul>
                                    </li>
                                @endguest
                            </ul>
                        </div>
                    </div>
                </nav>