<!DOCTYPE html>
<html class="fontawesome-i2svg-pending">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="csrf-token" content="{{ csrf_token() }}">

        <title>Shop-More - @yield('title')</title>

        <link href="{{URL::asset('css/app.css')}}" rel="stylesheet">
    </head>
    <body>



        <div class="wrapper">
            @include('layouts.sidebar')

            <!-- Page Content Holder -->
            <div id="content">
                @include('layouts.header')

                @if ($flash = session('message'))
      				<div class="alert alert-success">
        				{{ $flash }}
      				</div>
    			@endif

                <!-- Page Content -->
                <div class="container-fluid container-content-custom">
                  @include ('layouts.errors')
                    @yield('content')
                </div>
                 @include('layouts.footer')
            </div>

        </div>




        <script src="{{ asset('js/app.js') }}"></script>
         <script type="text/javascript">
              $(document).ready(function () {
     				$('#sidebarCollapse').on('click', function () {
                    	$('#sidebar, #content').toggleClass('active');
                    	$('.collapse.in').toggleClass('in');
                    	$('a[aria-expanded=true]').attr('aria-expanded', 'false');
                	});
            	});
         </script>

         @yield('script')
    </body>
</html>
