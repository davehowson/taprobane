@extends('layouts.master')
@section('title',$customer->name)

@section('content')
	<div class="container">
        <h3>
            Customer Information
        </h3>

        <div class="row">
            <div class="col-sm-8">
                
            <div>
                <table class="table table-bordered">
                    <tr>
                        <td>
                            Name :
                        </td>
                        <td>
                            {{ $customer->name }}
                        </td>
                    </tr>
                    <tr>
                        <td>
                            Email :
                        </td>
                        <td>
                            {{ $customer->email }}
                        </td>
                    </tr>
                    @foreach ($customer->addresses as $address)
                        @if ($address->isBilling == '1')
                            <tr>
                                <td>
                                    <strong>Billing Address :</strong>
                                </td>
                                <td>
                                    <span>{{ $address->address_one }}</span><br/>
                                    <span>{{ $address->address_two }}</span><br/>
                                    <span>{{ $address->city }}</span><br/>
                                    <span>{{ $address->state }}</span><br/>
                                    <span>{{ $address->postal_code}}</span><br/>
                                    <span>{{ $address->country }}</span><br/>
                                </td>
                            </tr>
                        @endif
                        @if ($address->isDelivery == '1')
                            <tr>
                                <td>
                                    <strong>Delivery Address :</strong>
                                </td>
                                <td>
                                    <span>{{ $address->address_one }}</span><br/>
                                    <span>{{ $address->address_two }}</span><br/>
                                    <span>{{ $address->city }}</span><br/>
                                    <span>{{ $address->state }}</span><br/>
                                    <span>{{ $address->postal_code}}</span><br/>
                                    <span>{{ $address->country }}</span><br/>
                                </td>
                            </tr>
                        @endif
                    @endforeach
                </table>
            </div>

            </div>


            <div class="col-sm-4">
                @if(Auth::guard('web')->check())
                    @if(Auth::user()->hasRole('Administrator'))
                        <div style="text-align: center; margin: 0 auto; border: 1px solid #dbd7d7; padding: 10px;">
                            <form method="GET" action="/customers/edit/{{$customer->id}}" style="margin: 5px;">
                                <button type="submit" class="btn btn-default" style="width: 75%">EDIT</button>
                            </form>
                            <form method="GET" action="/customers/delete/{{$customer->id}}" onsubmit="return confirm('Are you sure you want to delete?');" style="margin: 5px;">
                                <button type="submit" class="btn btn-default" style="width: 75%">DELETE</button>
                            </form>
                        </div>
                    @endif
                @endif

                
            </div>
        </div>


	</div>
@endsection