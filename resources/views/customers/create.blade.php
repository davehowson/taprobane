@extends('layouts.master')

@section('title','Create Customer')


@section('content')
	<div class="col-sm-8">

		<p class="text-secondary">Fields marked with * are required</p>

		<form method="POST" action="/customers/create" class="form-horizontal" id="createCustomer" role="form">
			{{ csrf_field() }}

			<div class="row">
				<fieldset>
					<legend>Customer Details</legend>

					<div class="form-group required">
						<label for="name" class=" col-sm-2 control-label">Name:</label>
						<div class="col-sm-10">
							<input type="name" class="form-control" placeholder="Name" id="name" name="name" required>
						</div>
					</div>

					<div class="form-group required">
						<label for="email" class="control-label col-sm-2">Email:</label>
						<div class="col-sm-10">
							<input type="text" class="form-control" placeholder="Email" id="email" name="email" required>
						</div>
					</div>

					<div class="form-group required">
						<label for="password" class="control-label col-sm-2">Password:</label>
						<div class="col-sm-4">
							<input type="password" class="form-control" placeholder="Password" id="password" name="password" required>
						</div>

						<label for="password_confirmation" class="control-label col-sm-2">Password Confirmation:</label>
						<div class="col-sm-4">
							<input type="password" class="form-control" placeholder="Password Confirmation" id="password_confirmation" name="password_confirmation" required>
						</div>
					</div>
				</fieldset>
			</div>

			<div class="row addresses" id="billing-address">
				<fieldset>
					<legend>Billing Address</legend>

					<div class="form-group required">
						<label for="billing-address-one" class="control-label col-sm-2">Line 1:</label>
						<div class="col-sm-10">
							<input type="text" class="form-control" placeholder="Address Line 1" id="billing-address-one" name="billing-address-one" required>
						</div>
					</div>

					<div class="form-group required">
						<label for="billing-address-two" class="control-label col-sm-2">Line 2:</label>
						<div class="col-sm-10">
							<input type="text" class="form-control" placeholder="Address Line 1" id="billing-address-two" name="billing-address-two">
						</div>
					</div>

					<div class="form-group required">
						<label for="billing-city" class="control-label col-sm-2">City:</label>
						<div class="col-sm-10">
							<input type="text" class="form-control" placeholder="City" id="billing-city" name="billing-city" required>
						</div>
					</div>

					<div class="form-group required">
						<label for="billing-state" class="control-label col-sm-2">State:</label>
						<div class="col-sm-4">
							<input type="text" class="form-control" placeholder="State" id="billing-state" name="billing-state">
						</div>
						<label for="billing-postal-code" class="control-label col-sm-2">Postalcode:</label>
						<div class="col-sm-4">
							<input type="text" class="form-control" placeholder="Postalcode" id="billing-postal-code" name="billing-postal-code">
						</div>
					</div>

					<div class="form-group required">
						<label for="billing-country" class="control-label col-sm-2">Country:</label>
						<div class="col-sm-10">
							<select class="form-control" id="billing-country" name="billing-country" form="createCustomer" required>
							</select>
						</div>
					</div>

					<div class="form-group centered-col">

				        <div class="checkbox col-sm-8">
				            <label style="font-size: 1.1em">
				                <input type="checkbox" id="isDelivery" value="1" name="isDelivery" checked>
				                <span class="cr"><i class="cr-icon fa fa-check"></i></span>
				                Same Address for Delivery?
				            </label>
				        </div>
						
					</div>
				</fieldset>
			</div>


			<div class="row addresses padding-on-top" id="delivery-address">
				<fieldset>
					<legend>Delivery Address</legend>

					<div class="form-group required">
						<label for="delivery-address-one" class="control-label col-sm-2">Line 1:</label>
						<div class="col-sm-10">
							<input type="text" class="form-control" placeholder="Address Line 1" id="delivery-address-one" name="delivery-address-one">
						</div>
					</div>

					<div class="form-group required">
						<label for="delivery-address-two" class="control-label col-sm-2">Line 2:</label>
						<div class="col-sm-10">
							<input type="text" class="form-control" placeholder="Address Line 1" id="delivery-address-two" name="delivery-address-two">
						</div>
					</div>

					<div class="form-group required">
						<label for="delivery-city" class="control-label col-sm-2">City:</label>
						<div class="col-sm-10">
							<input type="text" class="form-control" placeholder="City" id="delivery-city" name="delivery-city">
						</div>
					</div>

					<div class="form-group required">
						<label for="delivery-state" class="control-label col-sm-2">State:</label>
						<div class="col-sm-4">
							<input type="text" class="form-control" placeholder="State" id="delivery-state" name="delivery-state">
						</div>
						<label for="delivery-postal-code" class="control-label col-sm-2">Postalcode:</label>
						<div class="col-sm-4">
							<input type="text" class="form-control" placeholder="Postalcode" id="delivery-postal-code" name="delivery-postal-code">
						</div>
					</div>

					<div class="form-group required">
						<label for="delivery-country" class="control-label col-sm-2">Country:</label>
						<div class="col-sm-10">
							<input type="text" class="form-control" placeholder="Country" id="delivery-country" name="delivery-country">
						</div>
					</div>
				</fieldset>
			</div>

			

			<div class="padding-on-top">
				<button type="submit" class="btn btn-custom">Create</button>
			</div>
						
		</form>
	</div>
@stop

@section('script')
	<script>
	$("#delivery-address").hide();
	$("#isDelivery").click(function() {
	
	    if($(this).is(":checked")) {
	        $("#delivery-address").hide(200);
	         $('.delivery-addr').prop('required', false);
	    } else {
	        $("#delivery-address").show(300);
	        $('.delivery-addr').prop('required', true);
	    }
	});

	let dropdown = $('#billing-country');

	dropdown.empty();

	dropdown.append('<option selected="true" disabled>Choose Country</option>');
	dropdown.prop('selectedIndex', 0);

	const url = "{{ asset('storage/countries.json') }}";

	// Populate dropdown with list of countries
	$.getJSON(url, function (data) {
	  $.each(data, function (key, entry) {
	    dropdown.append($('<option></option>').attr('value', entry.code).text(entry.name));
	  });
	});
	</script>
@endsection

