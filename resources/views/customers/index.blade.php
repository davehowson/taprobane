@extends('layouts.master')

@section('title','Customers')

@section('content')
	<div class="row">
		<div class="col-sm-8">
			<form method="GET" action="/customers" class="navbar-form">
				<div class="input-group" style="width: 100%;">
					<input type="text" name="search" class="form-control" placeholder="Search" >
			 		<span class="input-group-btn">
    					<button class="btn btn-default-sm" type="submit">
        					<i class="fas fa-search"></i>
    					</button>
					</span>
				</div>
			</form>
		</div>
		@if(Auth::guard('web')->check())
		@if(Auth::user()->hasRole('Administrator'))
		<div class="col-sm-4">
			<form method="GET" action="/customers/create" class="navbar-form">
				<button type="submit" class="btn btn-custom" style="float: right;">Create Customer</button>
			</form>
		</div>
		@endif
		@endif
	</div>
	

	<table class="table table-striped">
		<thead class="thead-inverse">
			<tr>
				<th>Name</th>
				<th>Email</th>
			</tr>
		</thead>
		<tbody>
			@foreach ($customers as $customer)
				<tr class="clickable-row" data-url="/customers/{{ $customer->id }}">
					<td>{{ $customer->name }}</td>
					<td>{{ $customer->email }}</td>
				</tr>
			@endforeach
		</tbody>
	</table>
	{{ $customers->links() }}
@stop

@section('script')
	<script>
		jQuery(document).ready(function($){
			$(".clickable-row").click(function(){
				window.location = $(this).data("url");
			});
		});
	</script>
@endsection