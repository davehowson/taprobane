@extends('layouts.master')

@section('title','Users')

@section('content')

	<div class="row">
		<div class="col-sm-8">
			<form method="GET" action="/users" class="navbar-form">
				<div class="input-group" style="width: 100%;">
					<input type="text" name="search" class="form-control" placeholder="Search" >
			 		<span class="input-group-btn">
    					<button class="btn btn-default-sm" type="submit">
        					<i class="fas fa-search"></i>
    					</button>
					</span>
				</div>
			</form>
		</div>

		@if(Auth::user()->hasRole('Administrator'))
		<div class="col-sm-4">
			<form method="GET" action="/users/create" class="navbar-form">
				<button type="submit" class="btn btn-custom" style="float: right;">Create Account</button>
			</form>
		</div>
		@endif
	</div>
	

	<table class="table table-striped">
		<thead class="thead-inverse">
			<tr>
				<th>Name</th>
				<th>Email</th>
				<th>Role</th>
			</tr>
		</thead>
		<tbody>
			@foreach ($users as $user)
				<tr class="clickable-row" data-url="/users/{{ $user->id }}">
					<td>{{ $user->name }}</td>
					<td>{{ $user->email }}</td>
					<td>
						@foreach ($user->roles as $role)
							<span>{{$role->name}}</span>
						@endforeach
					</td>
				</tr>
			@endforeach
		</tbody>
	</table>
	{{ $users->links() }}
@stop

@section('script')
	<script>
		jQuery(document).ready(function($){
			$(".clickable-row").click(function(){
				window.location = $(this).data("url");
			});
		});
	</script>
@endsection