@extends('layouts.master')
@section('title',$user->name)

@section('content')

    <div class="container">
        <h3>
            User Information
        </h3>

        <div class="row">
            <div class="col-sm-8">
                
            <div>
                <table class="table table-bordered">
                    <tr>
                        <td>
                            Name :
                        </td>
                        <td>
                            {{ $user->name }}
                        </td>
                    </tr>
                    <tr>
                        <td>
                            Email :
                        </td>
                        <td>
                            {{ $user->email }}
                        </td>
                    </tr>

                    <tr>
                        <td>
                            Role :
                        </td>
                        <td>
                            @foreach ($user->roles as $role)
                                <span>{{$role->name}}</span>
                            @endforeach
                        </td>
                    </tr>
                </table>
            </div>

            </div>


            <div class="col-sm-4">
                @if(Auth::user()->hasRole('Administrator'))
                    <div style="text-align: center; margin: 0 auto; border: 1px solid #dbd7d7; padding: 10px;">
                        <form method="GET" action="/users/edit/{{$user->id}}" style="margin: 5px;">
                            <button type="submit" class="btn btn-default" style="width: 75%">EDIT</button>
                        </form>
                        <form method="GET" action="/users/delete/{{$user->id}}" onsubmit="return confirm('Are you sure you want to delete?');" style="margin: 5px;">
                            <button type="submit" class="btn btn-default" style="width: 75%">DELETE</button>
                        </form>
                    </div>
                @endif
            </div>
        </div>


    </div>
@endsection