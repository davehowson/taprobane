@extends('layouts.master')

@section('title','Create')


@section('content')
	<div class="col-sm-8">

		<p class="text-secondary">Fields marked with * are required</p>

		<form method="POST" action="/users/create" class="form-horizontal" role="form">
			{{ csrf_field() }}

			<div class="row">
				<fieldset>
					<legend>User Details</legend>

					<div class="form-group required">
						<label for="name" class="col-sm-2 control-label">Name:</label>
						<div class="col-sm-10">
							<input type="name" class="form-control" placeholder="Name" id="name" name="name" required>
						</div>
					</div>

					<div class="form-group required">
						<label for="email" class="control-label col-sm-2">Email:</label>
						<div class="col-sm-10">
							<input type="text" class="form-control" placeholder="Email" id="email" name="email" required>
						</div>
					</div>

					<div class="form-group required">
						<label for="password" class="control-label col-sm-2">Password:</label>
						<div class="col-sm-4">
							<input type="password" class="form-control" placeholder="Password" id="password" name="password" required>
						</div>

						<label for="password_confirmation" class="control-label col-sm-2">Password Confirmation:</label>
						<div class="col-sm-4">
							<input type="password" class="form-control" placeholder="Password Confirmation" id="password_confirmation" name="password_confirmation" required>
						</div>
					</div>

					<div class="form-group required">
		  				<label for="sel1" class="control-label col-sm-2">Select Role:</label>
		  				<div class="col-sm-4">
			  				<select class="form-control" id="sel1" name="role" required>
			  					<option value="" selected disabled hidden>Please select</option>
			    				<option value="1">Administrator</option>
			    				<option value="2">Moderator</option>
			  				</select>
			  			</div>
					</div>

				</fieldset>
			</div>



			<div class="padding-on-top">
				<button type="submit" class="btn btn-custom">Register</button>
			</div>
			
		</form>
	</div>
@endsection