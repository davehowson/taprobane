@extends ('layouts.master')

@section ('title','Stock Dispatch Log')

@section('content')
	
	<div>
		<table class="table">
			<thead>
				<tr>
					<th>Time</th>
					<th>Product</th>
					<th>Type</th>
					<th>Qty</th>
					<th>Price</th>
				</tr>
				@foreach ($logs as $log)
					<tr>
						<td>{{ $log->created_at }}</td>
						<td>{{ $log->stock->product->product_name }}</td>
						<td>{{ $log->type }}</td>
						<td>{{ $log->quantity }}</td>
						<td>{{ $log->price }}</td>
					</tr>
				@endforeach
			</thead>
		</table>
	</div>

@stop