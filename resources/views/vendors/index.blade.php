@extends('layouts.master')

@section('title','Vendors')

@section('content')

	<div class="row">
		<div class="col-sm-8">
			<form method="GET" action="/vendors" class="navbar-form">
				<div class="input-group" style="width: 100%;">
					<input type="text" name="search" class="form-control" placeholder="Search" >
			 		<span class="input-group-btn">
    					<button class="btn btn-default-sm" type="submit">
        					<i class="fa fa-search"></i>
    					</button>
					</span>
				</div>
			</form>
		</div>

		@if(Auth::user()->hasRole('Administrator'))
		<div class="col-sm-4">
			<form method="GET" action="/vendors/create" class="navbar-form">
				<button type="submit" class="btn btn-custom" style="float: right;">Create Vendor</button>
			</form>
		</div>
		@endif
	</div>
	

	<table class="table table-striped">
		<thead class="thead-inverse">
			<tr>
				<th>Name</th>
				<th>Contact Number</th>
				<th>Credit Period</th>
				<th>Rating</th>
				<th>Products</th>
			</tr>
		</thead>
		<tbody>
			@foreach ($vendors as $vendor)
				<tr class="clickable-row" data-url="/vendors/{{ $vendor->id }}">
					<td>{{ $vendor->name }}</td>
					<td>{{ $vendor->contact_number }}</td>
					<td>{{ $vendor->credit_period }}</td>
					<td>{{ $vendor->rating }}</td>
					<td>
						@foreach ($vendor->products as $products)
							<span>{{ $products->product_name }} </span><span class="text-primary">|</span>
						@endforeach
					</td>
				</tr>
			@endforeach
		</tbody>
	</table>
	{{ $vendors->links() }}
@stop

@section('script')
	<script>
		jQuery(document).ready(function($){
			$(".clickable-row").click(function(){
				window.location = $(this).data("url");
			});
		});
	</script>
@endsection