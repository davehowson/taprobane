@extends('layouts.master')
@section('title',$vendor->name)

@section('content')
    <div class="container">
        <h3>
            Vendor Information
        </h3>

        <div class="row">
            <div class="col-sm-8">
                
            <div>
                <table class="table table-bordered">
                    <tr>
                        <td>
                            Name :
                        </td>
                        <td>
                            {{ $vendor->name }}
                        </td>
                    </tr>
                    <tr>
                        <td>
                            Contact Number :
                        </td>
                        <td>
                            {{ $vendor->contact_number }}
                        </td>
                    </tr>
                    <tr>
                        <td>
                            Credit Period :
                        </td>
                        <td>
                            {{ $vendor->credit_period }}
                        </td>
                    </tr>
                    <tr>
                        <td>
                            Rating :
                        </td>
                        <td>
                            {{ $vendor->rating }}
                        </td>
                    </tr>
                    @foreach ($vendor->addresses as $address)
                        @if ($address->isBilling == '1')
                            <tr>
                                <td>
                                    <strong>Billing Address :</strong>
                                     @if ($address->isDefault == '1')
                                        <span><br/>(Default)</span>
                                    @endif
                                </td>
                                <td>
                                    <span>{{ $address->address_one }}</span><br/>
                                    <span>{{ $address->address_two }}</span><br/>
                                    <span>{{ $address->city }}</span><br/>
                                    <span>{{ $address->state }}</span><br/>
                                    <span>{{ $address->postal_code}}</span><br/>
                                    <span>{{ $address->country }}</span><br/>
                                </td>
                            </tr>
                        @endif
                        @if ($address->isDelivery == '1')
                            <tr>
                                <td>
                                    <strong>Delivery Address :</strong>
                                     @if ($address->isDefault == '1')
                                        <span><br/>(Default)</span>
                                    @endif
                                </td>
                                <td>
                                    <span>{{ $address->address_one }}</span><br/>
                                    <span>{{ $address->address_two }}</span><br/>
                                    <span>{{ $address->city }}</span><br/>
                                    <span>{{ $address->state }}</span><br/>
                                    <span>{{ $address->postal_code}}</span><br/>
                                    <span>{{ $address->country }}</span><br/>
                                </td>
                            </tr>
                        @endif
                    @endforeach
                </table>
            </div>

            </div>


            <div class="col-sm-4">
                @if(Auth::user()->hasRole('Administrator'))
                    <div style="text-align: center; margin: 0 auto; border: 1px solid #dbd7d7; padding: 10px;">
                        <form method="GET" action="/vendors/edit/{{$vendor->id}}" style="margin: 5px;">
                            <button type="submit" class="btn btn-default" style="width: 75%">EDIT</button>
                        </form>
                        <form method="GET" action="/vendors/delete/{{$vendor->id}}" onsubmit="return confirm('Are you sure you want to delete?');" style="margin: 5px;">
                            <button type="submit" class="btn btn-default" style="width: 75%">DELETE</button>
                        </form>
                    </div>
                @endif
            </div>
        </div>


    </div>
@endsection