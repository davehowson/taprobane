@extends('layouts.master')

@section('title','Edit Vendor')
@section('content')
<div class="col-sm-8">
	<form action="/vendors/edit/{{$vendor->id}}" method="POST" class="form-horizontal" role="form">
		{{ csrf_field() }}

		<div class="row">
			<fieldset>
				<legend>Customer Details</legend>
		
				<div class="form-group">
					<label for="name" class="col-sm-2 control-label">Name:</label>
					<div class="col-sm-10">
						<input type="name" class="form-control" id="name" name="name" value="{{$vendor->name}}">
					</div>
				</div>

				<div class="form-group">
					<label for="contact_number" class="col-sm-2 control-label">Contact:</label>
					<div class="col-sm-4">
						<input type="text" class="form-control" id="contact_number" name="contact_number" value="{{$vendor->contact_number}}">
					</div>
				</div>

				<div class="form-group">
					<label for="credit_period" class="col-sm-2 control-label">Credit Period:</label>
					<div class="col-sm-4">
						<input type="text" class="form-control" id="credit_period" name="credit_period" value="{{$vendor->credit_period}}">
					</div>
				</div>

				<div class="form-group">
					<label for="rating" class="col-sm-2 control-label">Rating:</label>
					<div class="col-sm-4">
						<input type="text" class="form-control" id="rating" name="rating" value="{{$vendor->rating}}">
					</div>
				</div>
			</fieldset>
		</div>


			<!--ADDRESSES-->
			@if (count($billingAddresses) > 0)
				@foreach ($billingAddresses as $billingAddress)
						<div class="row addresses" id="billing-address">
							<fieldset>
								<legend>Billing Address</legend>

								<input name="billing-address-id" value="{{$billingAddress->id}}" readonly hidden>

								<div class="form-group">
									<label for="billing-address-one" class="control-label col-sm-2">Line 1:</label>
									<div class="col-sm-10">
										<input type="text" class="form-control" value="{{$billingAddress->address_one}}" id="billing-address-one" name="billing-address-one" required>
									</div>
								</div>

								<div class="form-group">
									<label for="billing-address-two" class="control-label col-sm-2">Line 2:</label>
									<div class="col-sm-10">
										<input type="text" class="form-control" value="{{$billingAddress->address_two}}" id="billing-address-two" name="billing-address-two">
									</div>
								</div>

								<div class="form-group">
									<label for="billing-city" class="control-label col-sm-2">City:</label>
									<div class="col-sm-10">
										<input type="text" class="form-control" value="{{$billingAddress->city}}" id="billing-city" name="billing-city" required>
									</div>
								</div>

								<div class="form-group">
									<label for="billing-state" class="control-label col-sm-2">State:</label>
									<div class="col-sm-4">
										<input type="text" class="form-control" value="{{$billingAddress->state}}" id="billing-state" name="billing-state">
									</div>
									<label for="billing-postal-code" class="control-label col-sm-2">Postalcode:</label>
									<div class="col-sm-4">
										<input type="text" class="form-control" value="{{$billingAddress->postal_code}}" id="billing-postal-code" name="billing-postal-code" required>
									</div>
								</div>

								<div class="form-group">
									<label for="billing-country" class="control-label col-sm-2">Country:</label>
									<div class="col-sm-10">
										<input type="text" class="form-control" value="{{$billingAddress->country}}" id="billing-country" name="billing-country" required>
									</div>
								</div>

								<div class="form-group centered-col">

							        <div class="checkbox col-sm-8">
							            <label style="font-size: 1.1em">
							                <input type="checkbox" id="isDelivery" value="1" name="isDelivery" @if ($billingAddress->isDelivery) checked @endif>
							                <span class="cr"><i class="cr-icon fa fa-check"></i></span>
							                Same Address for Delivery?
							            </label>
							        </div>
							     </div>
							</fieldset>
						</div>
					@endforeach
				@endif


				@if (count($deliveryAddresses) > 0)
					@foreach ($deliveryAddresses as $deliveryAddress)
						<div class="row addresses" id="delivery-address">
							<fieldset>
								<legend>Delivery Address</legend>

								<input name="id" value="{{$deliveryAddress->id}}" readonly hidden>

								<div class="form-group">
									<label for="delivery-address-one" class="control-label col-sm-2">Line 1:</label>
									<div class="col-sm-10">
										<input type="text" class="form-control" value="{{$deliveryAddress->address_one}}" id="delivery-address-one" name="delivery-address-one">
									</div>
								</div>

								<div class="form-group">
									<label for="delivery-address-two" class="control-label col-sm-2">Line 2:</label>
									<div class="col-sm-10">
										<input type="text" class="form-control" value="{{$deliveryAddress->address_two}}" id="delivery-address-two" name="delivery-address-two">
									</div>
								</div>

								<div class="form-group">
									<label for="delivery-city" class="control-label col-sm-2">City:</label>
									<div class="col-sm-10">
										<input type="text" class="form-control" value="{{$deliveryAddress->city}}" id="delivery-city" name="delivery-city">
									</div>
								</div>

								<div class="form-group">
									<label for="delivery-state" class="control-label col-sm-2">State:</label>
									<div class="col-sm-4">
										<input type="text" class="form-control" value="{{$deliveryAddress->state}}" id="delivery-state" name="delivery-state">
									</div>
									<label for="delivery-postal-code" class="control-label col-sm-2">Postalcode:</label>
									<div class="col-sm-4">
										<input type="text" class="form-control" value="{{$deliveryAddress->postal_code}}" id="delivery-postal-code" name="delivery-postal-code">
									</div>
								</div>

								<div class="form-group">
									<label for="delivery-country" class="control-label col-sm-2">Country:</label>
									<div class="col-sm-10">
										<input type="text" class="form-control" value="{{$deliveryAddress->country}}" id="delivery-country" name="delivery-country">
									</div>
								</div>
							</fieldset>
						</div>
					@endforeach
				@endif


			<div>
				<button type="submit" class="btn btn-custom">Submit</button>
			</div>

	</form>
</div>

@stop

@section('script') {{-- needs work --}}
	<script>
	$(function(){
		checkCheckbox();
	});


	$("#isDelivery").click(function() {
		checkCheckbox();
	});

	function checkCheckbox(){
		 if($('#isDelivery').is(":checked")) {
	        $("#delivery-address").hide(200);
	         $('.delivery-addr').prop('required', false);
	    } else {
	        $("#delivery-address").show(300);
	        $('.delivery-addr').prop('required', true);
	    }
	}


	</script>
@endsection