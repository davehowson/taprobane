@extends('layouts.master')

@section('title','Create Vendor')


@section('content')
	<div class="col-sm-8">

		<p class="text-secondary">Fields marked with * are required</p>

		<form method="POST" action="/vendors/create" class="form-horizontal" role="form" id="vendor-form">
			{{ csrf_field() }}
			<div class="row">
				<fieldset>
					<legend>Vendor Details</legend>

					<div class="form-group required">
						<label for="name" class=" col-sm-2 control-label">Name:</label>
						<div class="col-sm-10">
							<input type="name" class="form-control" placeholder="Name" id="name" name="name" required>
						</div>
					</div>

					<div class="form-group required">
						<label for="contact_number" class="control-label col-sm-2">Contact No.:</label>
						<div class="col-sm-4">
							<input type="text" class="form-control" placeholder="Contact No." id="contact_number" name="contact_number" required>
						</div>

						<label for="credit_period" class="control-label col-sm-2">Credit Period:</label>
						<div class="col-sm-4">
							<input type="text" class="form-control" placeholder="Credit Period" id="credit_period" name="credit_period" required>
						</div>
					</div>

					<div class="form-group required">
						<label for="rating" class="control-label col-sm-2">Rating:</label>
						<div class="col-sm-10">
							<input type="text" class="form-control" placeholder="Rating" id="rating" name="rating" required>
						</div>
					</div>
				</fieldset>
			</div>

			<div class="row addresses" id="billing-address">
				<fieldset>
					<legend>Billing Address</legend>

					<div class="form-group required">
						<label for="billing-address-one" class="control-label col-sm-2">Line 1:</label>
						<div class="col-sm-10">
							<input type="text" class="form-control" placeholder="Address Line 1" id="billing-address-one" name="billing-address-one" required>
						</div>
					</div>

					<div class="form-group required">
						<label for="billing-address-two" class="control-label col-sm-2">Line 2:</label>
						<div class="col-sm-10">
							<input type="text" class="form-control" placeholder="Address Line 1" id="billing-address-two" name="billing-address-two">
						</div>
					</div>

					<div class="form-group required">
						<label for="billing-city" class="control-label col-sm-2">City:</label>
						<div class="col-sm-10">
							<input type="text" class="form-control" placeholder="City" id="billing-city" name="billing-city" required>
						</div>
					</div>

					<div class="form-group required">
						<label for="billing-state" class="control-label col-sm-2">State:</label>
						<div class="col-sm-4">
							<input type="text" class="form-control" placeholder="State" id="billing-state" name="billing-state">
						</div>
						<label for="billing-postal-code" class="control-label col-sm-2">Postalcode:</label>
						<div class="col-sm-4">
							<input type="text" class="form-control" placeholder="Postalcode" id="billing-postal-code" name="billing-postal-code">
						</div>
					</div>

					<div class="form-group required">
						<label for="billing-country" class="control-label col-sm-2">Country:</label>
						<div class="col-sm-10">
							<input type="text" class="form-control" placeholder="Country" id="billing-country" name="billing-country" required>
						</div>
					</div>

					<div class="form-group centered-col">

						<div class="checkbox col-sm-4">
				            <label style="font-size: 1.1em">
				                <input type="checkbox" id="isDefault-billing" value="1" name="isDefault-billing" checked>
				                <span class="cr"><i class="cr-icon fa fa-check"></i></span>
				                Is this Default?
				            </label>
				        </div>

				        <div class="checkbox col-sm-8">
				            <label style="font-size: 1.1em">
				                <input type="checkbox" id="isDelivery" value="1" name="isDelivery" checked>
				                <span class="cr"><i class="cr-icon fa fa-check"></i></span>
				                Same Address for Delivery?
				            </label>
				        </div>
						
					</div>
				</fieldset>
			</div>


			<div class="row addresses" id="delivery-address">
				<fieldset>
					<legend>Delivery Address</legend>

					<div class="form-group required">
						<label for="delivery-address-one" class="control-label col-sm-2">Line 1:</label>
						<div class="col-sm-10">
							<input type="text" class="form-control delivery-addr" placeholder="Address Line 1" id="delivery-address-one" name="delivery-address-one">
						</div>
					</div>

					<div class="form-group">
						<label for="delivery-address-two" class="control-label col-sm-2">Line 2:</label>
						<div class="col-sm-10">
							<input type="text" class="form-control delivery-addr" placeholder="Address Line 1" id="delivery-address-two" name="delivery-address-two">
						</div>
					</div>

					<div class="form-group required">
						<label for="delivery-city" class="control-label col-sm-2">City:</label>
						<div class="col-sm-10">
							<input type="text" class="form-control delivery-addr" placeholder="City" id="delivery-city" name="delivery-city">
						</div>
					</div>

					<div class="form-group required">
						<label for="delivery-state" class="control-label col-sm-2">State:</label>
						<div class="col-sm-4">
							<input type="text" class="form-control delivery-addr" placeholder="State" id="delivery-state" name="delivery-state">
						</div>
						<label for="delivery-postal-code" class="control-label col-sm-2">Postalcode:</label>
						<div class="col-sm-4">
							<input type="text" class="form-control delivery-addr" placeholder="Postalcode" id="delivery-postal-code" name="delivery-postal-code">
						</div>
					</div>

					<div class="form-group required">
						<label for="delivery-country" class="control-label col-sm-2">Country:</label>
						<div class="col-sm-10">
							<input type="text" class="form-control delivery-addr" placeholder="Country" id="delivery-country" name="delivery-country">
						</div>
					</div>

					<div class="form-group centered-col">

						<div class="checkbox col-sm-4">
				            <label style="font-size: 1.1em">
				                <input type="checkbox" id="isDefault-delivery" value="1" name="isDefault-delivery" checked>
				                <span class="cr"><i class="cr-icon fas fa-check"></i></span>
				                Is this Default?
				            </label>
				        </div>
						
					</div>
				</fieldset>
			</div>

			<div>
				<h3>Select Products</h3>
				<table class="table">
					<tbody class="table-body">
						<tr>
							<td>
								<div>
			                        <select class="form-control vendor-select form-select" id="product1" name="product[1]">
			                            <option value="" selected disabled hidden>Select Product</option>
			                                @foreach ($products as $product)
			                                    <option value="{{ $product->id }}">{{ $product->product_name }}</option>
			                                @endforeach
			                        </select>
			                    </div>
							</td>
							<td> <button id="b1" class="btn add_field_button" type="button">+</button></td>
						</tr>
					</tbody>
				</table>
                    

    					  
                      
			</div>

			

			<div>
				<button type="submit" class="btn btn-custom">Create</button>
			</div>
			
		</form>
	</div>
@stop

@section('script') {{-- needs work --}}
	<script>
		$("#delivery-address").hide();

		$("#isDelivery").click(function() {
		    if($(this).is(":checked")) {
		        $("#delivery-address").hide(200);
		        $('.delivery-addr').prop('required', false)
		    } else {
		        $("#delivery-address").show(300);
		        $('.delivery-addr').prop('required', true)
		    }
		});

		var max_fields      = 9; //maximum input boxes allowed
	    var wrapper         = $(".table-body"); //Fields wrapper
	    var add_button      = $(".add_field_button"); //Add button ID


	    function declareAndAppend(){
	    

	        

	    	var productTD 			='<tr><td><div><select class="form-control vendor-select form-select" id="product'+ x +'" name="product['+ x +']"><option value="" selected disabled hidden>Select Product</option>@foreach ($products as $product)<option value="{{ $product->id }}">{{ $product->product_name }}</option>@endforeach</select></div></td>'


	    	var btnRemove		= '<td><button id="b'+ x +'" class="btn remove_field_button" type="button">-</button></td></tr>'


	    	var toAppend		= productTD.concat(btnRemove);



	    	return toAppend;
	    }


	    var x = 1; //initlal text box count
	    $(add_button).click(function(e){ //on add input button click
	        e.preventDefault();
	        if(x < max_fields){ //max input box allowed
	            x++; //text box increment
	            $(wrapper).append(declareAndAppend()); //add input box
	        }else{
	            $(add_button).prop("disabled",true); //disables button after max rows
	        }
	    });
	    
	    $(wrapper).on("click",".remove_field_button", function(e){      //user click on remove text
	        e.preventDefault(); $(this).parents('tr').remove(); x--;
	         $(add_button).prop("disabled",false);  //enables button incase disabled
	         calculateTotal();
	    });

	    $('#vendor-form').on('submit', function(e){
	    	var items = new Array();
	    	$('.vendor-select').each(function(){
	    		var item = $(this).val();
	    		if(jQuery.inArray(item ,items) !== -1){
	    			alert("ERROR: Duplicate Products");
	    			e.preventDefault();
	    		} else {
	    			items.push(item);
	    		}
	    	});
	    })
	</script>
@endsection