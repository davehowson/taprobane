@extends('layouts.master')

@section('title','Products')

@section('content')

	<div class="row">
		<div class="col-sm-8">
			<form method="GET" action="/products" class="navbar-form">
				<div class="input-group" style="width: 100%;">
					<input type="text" name="search" class="form-control" placeholder="Search" >
			 		<span class="input-group-btn">
    					<button class="btn btn-default-sm" type="submit">
        					<i class="fas fa-search"></i>
    					</button>
					</span>
				</div>
			</form>
		</div>

		@if(Auth::user()->hasRole('Administrator'))
		<div class="col-sm-4">
			<form method="GET" action="/products/create" class="navbar-form">
				<button type="submit" class="btn btn-custom" style="float: right;">Create Product</button>
			</form>
		</div>
		@endif
	</div>
	

	<table class="table table-striped">
		<thead class="thead-inverse">
			<tr>
				<th>Product Name</th>
				<th>Description</th>
				<th>Category</th>
				<th>Price</th>
			</tr>
		</thead>
		<tbody>
			@foreach ($products as $product)
				<tr class="clickable-row" data-url="/products/{{ $product->id }}">
					
					<td>{{ $product->product_name }}</td>
					<td>{{ $product->product_description }}</td>
					<td>{{ $product->category->category_name }}</td>
					<td>{{ $product->product_price }}</td>
				</tr>
			@endforeach
		</tbody>
	</table>
	{{ $products->links() }}
@stop

@section('script')
	<script>
		jQuery(document).ready(function($){
			$(".clickable-row").click(function(){
				window.location = $(this).data("url");
			});
		});
	</script>
@endsection