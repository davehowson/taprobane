@extends('layouts.master')
@section('title',$product->product_name)

@section('content')

    <div class="container">

        <div class="row">
            <div class="col-sm-8">
                
            <div>
                <table class="table table-bordered">
                    <tr>
                        <td>
                            Product Name :
                        </td>
                        <td>
                            {{ $product->product_name }}
                        </td>
                    </tr>
                    <tr>
                        <td>
                            Description :
                        </td>
                        <td>
                            {{ $product->product_description }}
                        </td>
                    </tr>

                    <tr>
                        <td>
                            Category :
                        </td>
                        <td>
                            {{ $product->category->category_name }}
                        </td>
                    </tr>

                    <tr>
                        <td>
                            Price :
                        </td>
                        <td>
                            {{ $product->product_price }}
                        </td>
                    </tr>
                    <tr>
                        <td>
                            Image :
                        </td>
                        <td>
                            <img src="{{ asset('storage/products/'.$product->product_image) }}" width="200px" height="200px">
                        </td>
                    </tr>
                </table>
            </div>

            </div>


            <div class="col-sm-4">
                @if(Auth::user()->hasRole('Administrator'))
                    <div style="text-align: center; margin: 0 auto; border: 1px solid #dbd7d7; padding: 10px;">
                        <form method="GET" action="/products/edit/{{$product->id}}" style="margin: 5px;">
                            <button type="submit" class="btn btn-default" style="width: 75%">EDIT</button>
                        </form>
                        <form method="GET" action="/products/delete/{{$product->id}}" onsubmit="return confirm('Are you sure you want to delete?');" style="margin: 5px;">
                            <button type="submit" class="btn btn-default" style="width: 75%">DELETE</button>
                        </form>
                    </div>
                @endif
            </div>
        </div>


    </div>
@endsection