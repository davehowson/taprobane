@extends('layouts.master')

@section('title','Categories')

@section('content')

	<div class="row">
		<div class="col-sm-8">
			<form method="GET" action="/categories" class="navbar-form">
				<div class="input-group" style="width: 100%;">
					<input type="text" name="search" class="form-control" placeholder="Search" >
			 		<span class="input-group-btn">
    					<button class="btn btn-default-sm" type="submit">
        					<i class="fa fa-search"></i>
    					</button>
					</span>
				</div>
			</form>
		</div>

		@if(Auth::user()->hasRole('Administrator'))
		<div class="col-sm-4">
			<form method="GET" action="/categories/create" class="navbar-form">
				<button type="submit" class="btn btn-custom" style="float: right;">Create Category</button>
			</form>
		</div>
		@endif
	</div>
	

	<table class="table table-striped">
		<thead class="thead-inverse">
			<tr>
				<th>Category Name</th>
				<th>Category Description</th>
			</tr>
		</thead>
		<tbody>
			@foreach ($categories as $category)
				<tr class="clickable-row" data-url="/categories/{{ $category->id }}">
					<td>{{ $category->category_name }}</td>
					<td>{{ $category->category_description }}</td>
				</tr>
			@endforeach
		</tbody>
	</table>
	{{ $categories->links() }}
@stop

@section('script')
	<script>
		jQuery(document).ready(function($){
			$(".clickable-row").click(function(){
				window.location = $(this).data("url");
			});
		});
	</script>
@endsection