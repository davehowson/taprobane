@extends('layouts.master')

@section('title','Edit Category')
@section('content')
	<div class="col-sm-8">
		<form action="/categories/edit/{{$category->id}}" method="POST" class="form-horizontal" role="form">
			{{ csrf_field() }}
			<div class="row">
				<fieldset>
					<legend>Category Details</legend>

					<div class="form-group">
						<label for="category_name" class="col-sm-2 control-label">Category Name:</label>
						<div class="col-sm-10">
							<input type="text" class="form-control" id="category_name" name="category_name" value="{{$category->category_name}}">
						</div>
					</div>

					<div class="form-group">
						<label for="category_description" class="col-sm-2 control-label">Category Description:</label>
						<div class="col-sm-10">
							<input type="text" class="form-control" id="category_description" name="category_description" value="{{$category->category_description}}">
						</div>
					</div>

						<div class="padding-on-top">
							<button type="submit" class="btn btn-custom">Submit</button>
						</div>
				</fieldset>
			</div>
		</form>
	</div>

@stop