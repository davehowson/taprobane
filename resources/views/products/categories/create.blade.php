@extends('layouts.master')

@section('title','Create Category')


@section('content')
	<div class="col-sm-8">

		<p class="text-secondary">Fields marked with * are required</p>

		<form method="POST" action="/categories/create" class="form-horizontal" role="form">
			{{ csrf_field() }}

			<div class="row">
				<fieldset>
					<legend>Product Details</legend>

					<div class="form-group required">
						<label for="category_name" class="col-sm-2 control-label">Name:</label>
						<div class="col-sm-10">
							<input type="text" class="form-control" id="category_name" name="category_name" required>
						</div>
					</div>

					<div class="form-group required">
						<label for="category_description" class="col-sm-2 control-label">Description:</label>
						<div class="col-sm-10">
							<input type="text" class="form-control" id="category_description" name="category_description" required>
						</div>
					</div>

					<div class="padding-on-top">
						<button type="submit" class="btn btn-custom">Create</button>
					</div>
				</fieldset>
			</div>
			
		</form>
	</div>
@endsection