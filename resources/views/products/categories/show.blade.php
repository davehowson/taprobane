@extends('layouts.master')
@section('title',$category->category_name)

@section('content')

    <div class="container">

        <div class="row">
            <div class="col-sm-8">
                
            <div>
                <table class="table table-bordered">
                    <tr>
                        <td>
                            Category Name :
                        </td>
                        <td>
                            {{ $category->category_name }}
                        </td>
                    </tr>
                    <tr>
                        <td>
                            Description :
                        </td>
                        <td>
                            {{ $category->category_description }}
                        </td>
                    </tr>
                </table>
            </div>

            </div>


            <div class="col-sm-4">
                @if(Auth::user()->hasRole('Administrator'))
                    <div style="text-align: center; margin: 0 auto; border: 1px solid #dbd7d7; padding: 10px;">
                        <form method="GET" action="/categories/edit/{{$category->id}}" style="margin: 5px;">
                            <button type="submit" class="btn btn-default" style="width: 75%">EDIT</button>
                        </form>
                        <form method="GET" action="/categories/delete/{{$category->id}}" onsubmit="return confirm('Are you sure you want to delete?');" style="margin: 5px;">
                            <button type="submit" class="btn btn-default" style="width: 75%">DELETE</button>
                        </form>
                    </div>
                @endif
            </div>
        </div>


    </div>
@endsection