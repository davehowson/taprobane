@extends('layouts.master')

@section('title','Create Product')


@section('content')
	<div class="col-sm-8">

		<p class="text-secondary">Fields marked with * are required</p>

		<form method="POST" action="/products/create"  enctype="multipart/form-data" class="form-horizontal" role="form">
			{{ csrf_field() }}

			<div class="row">
				<fieldset>
					<legend>Product Details</legend>

					<div class="form-group required">
						<label for="product_name" class="col-sm-2 control-label">Product Name:</label>
						<div class="col-sm-10">
							<input type="text" class="form-control" id="product_name" name="product_name" required>
						</div>
					</div>

					<div class="form-group required">
						<label for="product_description" class="col-sm-2 control-label">Description:</label>
						<div class="col-sm-10">
							<input type="text" class="form-control" id="product_description" name="product_description" required>
						</div>
					</div>

					<div class="form-group required">
						<label for="product_price" class="col-sm-2 control-label">Price:</label>
						<div class="col-sm-4">
							<input type="number" class="form-control" id="product_price" name="product_price" required>
						</div>
					</div>

					<div class="form-group">
						<label for="product_image" class="col-sm-2 control-label">Image</label>
						<div class="col-sm-6">
							<input type="file" class="form-control" id="product_image" name="product_image">
						</div>
					</div>

					<div class="form-group required">
		  				<label for="product_category" class="col-sm-2 control-label">Select Category:</label>
		  				<div class="col-sm-6">
			  				<select class="form-control" id="product_category" name="product_category" required>
			  					<option value="" selected disabled hidden>Please select</option>
			    				@foreach ($categories as $category)
			    					<option value="{{ $category->id }}">{{ $category->category_name }}</option>
			    				@endforeach
			  				</select>
			  			</div>
					</div>

					<div class="padding-on-top">
						<button type="submit" class="btn btn-custom">Create</button>
					</div>
				</fieldset>
			</div>
		</form>
	</div>
@endsection