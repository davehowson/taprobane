@extends('layouts.master')

@section('title','Edit Product')
@section('content')
<div class="col-sm-8">

	<form action="/products/edit/{{$product->id}}" method="POST" enctype="multipart/form-data" class="form-horizontal" role="form">
		{{ csrf_field() }}

			<div class="row">
				<fieldset>
					<legend>Product Details</legend>

					<div class="form-group">
						<label class="col-sm-2 control-label" for="product_name" >Product Name:</label>
						<div class="col-sm-10">
							<input type="name" class="form-control" id="product_name" name="product_name" value="{{$product->product_name}}">
						</div>
					</div>

					<div class="form-group">
						<label class="col-sm-2 control-label" for="product_description">Contact Number:</label>
						<div class="col-sm-10">
							<input type="text" class="form-control" id="product_description" name="product_description" value="{{$product->product_description}}">
						</div>
					</div>

					<div class="form-group">
						<label class="col-sm-2 control-label" for="product_price">Price:</label>
						<div class="col-sm-4">
							<input type="text" class="form-control" id="product_price" name="product_price" value="{{$product->product_price}}">
						</div>
					</div>

					<div class="form-group">
						<label class="col-sm-2 control-label" for="image">Image</label>
						<div class="col-sm-6">
							<input type="file" class="form-control" id="image" name="product_image">
						</div>
					</div>

					<div class="form-group">
		  				<label class="col-sm-2 control-label" for="product_category">Select Category:</label>
		  				<div class="col-sm-6">
			  				<select class="form-control" id="product_category" name="product_category">
			  					<option value="" selected disabled hidden>{{ $product->category->category_name }}</option>
			    				@foreach ($categories as $category)
			    					<option value="{{ $category->id }}">{{ $category->category_name }}</option>
			    				@endforeach
			  				</select>
			  			</div>
					</div>


					<div class="padding-on-top">
						<button type="submit" class="btn btn-custom">Update</button>
					</div>
				</fieldset>
			</div>
	</form>
</div>

@stop