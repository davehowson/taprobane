@extends ('layouts.master')

@section ('title','Purchase Receipt')

@section('content')
	<h3>Shopping Cart</h3>
	<div class="table-div">
		<table class="table">
			<thead>
				<tr>
					<th>Product Name</th>
					<th>Price</th>
					<th>Vendor</th>
					<th>Qty</th>
					<th>Total</th>
				</tr>
			</thead>
			<tbody>
				@foreach ($lines as $line)
					<tr>
						<td>{{ $line->product->product_name }}</td>
						<td>{{ $line->product->product_price }}</td>
						<td>{{ $line->vendor->name }}</td>
						<td>{{ $line->quantity }}</td>
						<td id="total{{$line->id}}"></td>
					</tr>
				@endforeach
			</tbody>
		</table>
		<div class="pull-right">
			<div>
                    <label for="total-cost-purchase-receipt">Total</label>
                    <input type="text" class="form-control total-cost" id="total-cost-purchase-receipt" readonly value="{{ $line->purchase->total_cost }}">
            </div>

			<form method="GET" action="/dashboard">
				<button type="submit" class="btn btn-custom" id="purchase-receipt-button">Done</button>
			</form>
		</div>
	</div>
	
@stop

@section('script')

	<script>
		$(document).ready(function(){
			var lines = <?php echo json_encode($lines);?>;

			$.each(lines, function (i, elem) {
				var unitPrice = elem.unit_price;
				var qty       = elem.quantity;
				
				var total      = unitPrice * qty;
				
				var id        = elem.id;
				
				var idText    = '#total'+id;

    			$(idText).text(total);
			});
		});
	</script>

@stop