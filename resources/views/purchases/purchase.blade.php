@extends ('layouts.master')

@section ('title','Purchase')

@section ('content')
	<div class="table-div">

		<form method="POST" action="/purchase/submit"  enctype="multipart/form-data" class="form-inline" id="purchase-form">
			{{ csrf_field() }}


            <div class="form-group required" id="purchase-date">
                <label for="date" class="control-label">Date:</label>
                <input type="text" class="form-control" id="datepicker" name="date" required> 
            </div>

            <table class="table purchase-table">
                <thead>
                    <th class="form-select-td">Vendor</th>
                    <th class="form-select-td">Product</th>
                    <th class="form-numbers-td">Quantity</th>
                   {{--  <th class="form-numbers-td">Selling Price</th> --}}
                    <th class="form-numbers-td">Unit Price</th>
                    <th class="form-numbers-td">Total Price</th>
                </thead>
                <tbody class="table-body">
                    <tr>   
                        <td class="form-select-td">
                            <div class="form-group">
                                <select class="form-control vendor-select form-select" id="vendor1" name="vendor[1]">
                                    <option value="" selected disabled hidden>Select Vendor</option>
                                    @foreach ($vendors as $vendor)
                                        <option value="{{ $vendor->id }}" data-products="{{ $vendor->products }}">{{ $vendor->name }}</option>
                                    @endforeach
                                </select>
                            </div>
                        </td>

                        <td class="form-select-td">
        					<div class="form-group">
          						<select class="form-control product-select form-select" id="product1" name="product[1]" >
          							<option value="" selected disabled hidden>Select Product</option>
          						</select>
        					</div>
                        </td>

                        <td class="form-numbers-td">
        					<div class="form-group" >
        						<input type="number" min="0" class="form-control input-quantity form-numbers" id="quantity1" name="quantity[1]" required>
        					</div>
                        </td>

                        {{-- <td class="form-numbers-td">
                            <div class="form-group" >
                                <input type="text" class="form-control form-numbers" id="unit-price1" name="unitprice[1]" readonly>
                            </div>
                        </td> --}}

                        <td class="form-numbers-td">
        					<div class="form-group" >
        						<input type="text" class="form-control form-numbers" id="unit-price1" name="unitprice[1]" readonly>
        					</div>
                        </td>

                        <td class="form-numbers-td">
    					   <div class="form-group" >
    						  <input type="text" class="form-control total-price form-numbers" id="total-price1" name="total-price1" readonly>
    					   </div>
                        </td>

                        <td>
    					   <button id="b1" class="btn add_field_button" type="button">+</button>
                        </td>
    		
                    </tr>
                </tbody>
            </table>

            <div class="pull-right">
                <div>
                    <label for="total-cost-purchase">Total Cost</label>
                    <input type="text" class="form-control total-cost" id="total-cost-purchase" name="total-cost" readonly>
                </div>

                <div class="purchase-button-div">
                    <button type="submit" class="btn btn-custom">Purchase</button>
                </div>
            </div>
			
			
		</form>
	</div>

@stop

@section('script')
	<script>
	$(document).ready(function() {

	$.ajaxSetup({
    	headers: {
        	'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
    	}
	});


    var max_fields      = 9; //maximum input boxes allowed
    var wrapper         = $(".table-body"); //Fields wrapper
    var add_button      = $(".add_field_button"); //Add button ID


    function declareAndAppend(){
    

        

    	var vendorDiv 			='<tr><td><div class="form-group"><select class="form-control vendor-select form-select" id="vendor'+ x +'" name="vendor['+ x +']"><option value="" selected disabled hidden>Select Vendor</option>@foreach ($vendors as $vendor)<option value="{{ $vendor->id }}" data-products="{{ $vendor->products }}">{{ $vendor->name }}</option>@endforeach</select></div></td>'

        var productDiv          ='<td><div class="form-group"><select class="form-control product-select form-select" id="product'+ x +'" name="product['+ x +']" ><option value="" selected disabled hidden>Select Product</option></select></div></td>'


    	var quantityDiv 		='<td class="form-numbers-td"><div class="form-group" ><input type="number" min="0" class="form-control input-quantity form-numbers" id="quantity'+ x +'" name="quantity['+ x +']" required></div></td>'


    	var unitPriceDiv 		='<td class="form-numbers-td"><div class="form-group" ><input type="text" class="form-control form-numbers" id="unit-price'+ x +'" name="unitprice['+ x +']" readonly></div></td>'


    	var totalPriceDiv 		='<td class="form-numbers-td"><div class="form-group" ><input type="text" class="form-control total-price form-numbers" id="total-price'+ x +'" name="total-price'+ x +'" readonly></div></td>'


    	var btnRemove		= '<td><button id="b'+ x +'" class="btn remove_field_button" type="button">-</button></td></tr>'


    	var toAppend		= vendorDiv.concat(productDiv,quantityDiv,unitPriceDiv,totalPriceDiv,btnRemove);



    	return toAppend;
    }


    var x = 1; //initlal text box count
    $(add_button).click(function(e){ //on add input button click
        e.preventDefault();
        if(x < max_fields){ //max input box allowed
            x++; //text box increment
            $(wrapper).append(declareAndAppend()); //add input box
        }else{
            $(add_button).prop("disabled",true); //disables button after max rows
        }
    });
    
    $(wrapper).on("click",".remove_field_button", function(e){      //user click on remove text
        e.preventDefault(); $(this).parents('tr').remove(); x--;
         $(add_button).prop("disabled",false);  //enables button incase disabled
         calculateTotal();
    });


    $(wrapper).on('change','.vendor-select', function() {       //populate product on vendor change

    	//identify row and select box
    	var idSelect = this.id;

    	var id = idSelect[idSelect.length-1];

    	var selected = $(this).find('option:selected');

    	//displaying products
    	var productsList = selected.data('products'); 

    	var productWithId = '#product'+id;
    	$(productWithId).empty();
    	$.each(productsList, function(key, value) {              
        	$(productWithId).append($('<option>').text(value.product_name).attr('value', value.id).attr('data-price',value.product_price));   
    	});

        //display unit price of top product
        var unitPrice = productsList[0].product_price;
        var unitPriceId = "#unit-price" + id;
    	$(unitPriceId).val(unitPrice);

    	//clear quantity when vendor is selected
    	var qtyToClear = '#quantity'+id;

    	$(qtyToClear).val('');

 

	});

    $(wrapper).on('change','.product-select', function() {//populate price on product change
        //displaying unit price

        var idSelect = this.id;

        var id = idSelect[idSelect.length-1];

        var selected = $(this).find('option:selected');

        var unitPrice = selected.data('price');

        var unitPriceText = '#unit-price'

        var unitPriceConcatId = unitPriceText.concat(id);

        $(unitPriceConcatId).val(unitPrice);

        //clear quantity when product is selected
        var qtyToClear = '#quantity'+id;

        $(qtyToClear).val('');
    });



    $(wrapper).on('input', 'input', function(){
    
    	//used for calculating the total price per product and displaying it

    	var inputIdText = this.id;
    	
    	var inputId = inputIdText[inputIdText.length-1];

    	var inputQty = this.value;

    	var totalPriceText = '#total-price'+ inputId;

    	var unitPriceTextForTotal = '#unit-price'+ inputId;

    	var unitPriceForTotal = $(unitPriceTextForTotal).val();

    	var totalProductPrice = inputQty * unitPriceForTotal;

    	$(totalPriceText).val(totalProductPrice);


        calculateTotal();

	});

    function calculateTotal(){
        var netTotal = 0;

        $('.total-price').each(function(){
            var temp = $(this).val();
            if (isNaN(temp) || temp == ""){
                //this is for empty textboxes
                temp = 0;
            }
            var totalPrice = parseFloat(temp);
            netTotal = netTotal + totalPrice;
        })

        $('#total-cost-purchase').val(netTotal);
    }


});
	</script>
@endsection