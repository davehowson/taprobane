@extends ('layouts.master')

@section ('title','Invoice')

@section ('content')
	<div class="table-div">
		<h2>Invoice Order</h2>

		<form method="POST" action="/invoice/submit"  enctype="multipart/form-data" class="form-inline">
			{{ csrf_field() }}

			<table class="table invoice-table">
                <thead>
                    <th class="form-select-td">Product</th>
                    <th id="stocks-header">Stocks Left</th>
                    <th class="form-numbers-td">Quantity</th>
                    <th class="form-numbers-td">Unit Price</th>
                    <th class="form-numbers-td">Total Price</th>
                </thead>
                <tbody>
    				<tr>
                        <td class="form-select-td">
                            <div class="form-group">
                                <select class="form-control form-select product-select " id="product1" name="product[1]">
                                    <option value="" selected disabled hidden>Select Product</option>
                                    @foreach ($products as $product)
                                        <option value="{{ $product->id }}" data-foo="{{ $product->product_price }}" data-bar="{{ $product->vendors }}" data-qty="{{ $product->stock->quantity}}" >{{ $product->product_name }}</option>
                                    @endforeach
                                </select>
                            </div>
                        </td>

                        <td class="stocks-td"><span id="stocksLeft1"></span></td>
    					
                        <td class="form-numbers-td">
                            <div class="form-group" >
                                <input type="number" class="form-control form-numbers input-quantity" id="quantity1" name="quantity[1]" required>
                            </div>
                        </td>
    					
                        <td class="form-numbers-td">
                            <div class="form-group" >
                                <input type="text" class="form-control form-numbers" id="unit-price1" name="unitprice[1]" readonly>
                            </div>
                        </td>
    					
                        <td class="form-numbers-td">
                            <div class="form-group" >
                                <input type="text" class="form-control form-numbers total-price" id="total-price1" name="total-price1" readonly>
                            </div>
                        </td>

    					<td>
                            <button id="b1" class="btn add_field_button" type="button">+</button>
                        </td>
    				</tr>
                </tbody>
			</table>

            <div class="form-group" >
                    <label for="delivery-address">Delivery Address</label>
                    <div>
                        @foreach(Auth::user()->addresses as $address)
                        	@if($address->isDelivery == '1')
                        		<div class="well" style="float: right; margin: 0 15px 10px 15px;">
                        			<input type="radio" class="form-control" id="delivery-address" name="delivery-address" value="{{ $address->id }}" @if($address->isDefault == '1') checked="checked" @endif>
                        			<br/>
                        			@if($address->isDefault == '1')
                        			<span style="color: red">Default</span><br/>
                        			@endif
                            		{{ $address->address_one }}<br/>
                            		{{ $address->address_two }}<br/>
                            		{{ $address->city }}<br/>
                            		{{ $address->state }}<br/>
                        			{{ $address->postal_code }}<br/>
                        			{{ $address->country }}
                        		</div>
                        		
                        	@endif
                       	@endforeach
                    </div>  
            </div>

            <div class="pull-right">
                <div>
                    <label for="total-cost-invoice">Total Cost</label>
                    <input type="text" class="form-control total-cost" id="total-cost-invoice" name="total-cost" readonly>
                </div>

                

                <div class="purchase-button-div">
                    <button type="submit" class="btn btn-custom">Purchase</button>
                </div>
            </div>
			
			
		</form>
	</div>

@stop

@section('script')
	<script>
	$(document).ready(function() {

	
	$.ajaxSetup({
    	headers: {
        	'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
    	}
	});


    var max_fields      = 10; //maximum input boxes allowed
    var wrapper         = $(".invoice-table"); //Fields wrapper
    var add_button      = $(".add_field_button"); //Add button ID

    

    function declareAndAppend(){
    

        var productsDiv    = '<tr><td class="form-select-td"><div class="form-group"><select class="form-control form-select product-select" id="product'+ x +'" name="product['+ x +']"><option value="" selected disabled hidden>Select Product</option>@foreach ($products as $product)<option value="{{ $product->id }}" data-foo="{{ $product->product_price }}" data-bar="{{ $product->vendors }}" data-qty="{{ $product->stock->quantity}}" >{{ $product->product_name }}</option>@endforeach</select></div></td>'

        var stocksLeftSpan = '<td class="stocks-td"><span id="stocksLeft' + x + '"></span></td>'
        
        
        var quantityDiv    = '<td class="form-numbers-td"><div class="form-group" ><input type="number" class="form-control form-numbers input-quantity" id="quantity'+ x +'" name="quantity['+ x +']" required></div></td>'
        
        
        var unitPriceDiv   = '<td class="form-numbers-td"><div class="form-group" ><input type="text" class="form-control form-numbers" id="unit-price' + x + '"name="unitprice['+ x +']" readonly></div></td>'
        
        
        var totalPriceDiv  = '<td class="form-numbers-td"><div class="form-group" ><input type="text" class="form-control form-numbers total-price" id="total-price' + x + '" name="total-price'+ x +'" readonly></div></td>'
        
        
        var btnRemove      = '<td><button id="b2" class="btn remove_field_button" type="button">-</button></td></tr>'
        
        
        var toAppend       = productsDiv.concat(stocksLeftSpan,quantityDiv,unitPriceDiv,totalPriceDiv,btnRemove);



    	return toAppend;
    }


    var x = 1; //initlal text box count
    $(add_button).click(function(e){ //on add input button click
        e.preventDefault();
        if(x < max_fields){ //max input box allowed
            x++; //text box increment
            $(wrapper).append(declareAndAppend()); //add input box
        }else{
            $(add_button).prop("disabled",true); //disables button after max rows
        }
    });
    
    $(wrapper).on("click",".remove_field_button", function(e){ //user click on remove text
        e.preventDefault(); $(this).parents('tr').remove(); x--;
         $(add_button).prop("disabled",false);  //enables button incase disabled
         calculateTotal();
    });




    $(wrapper).on('change','.product-select', function() {//unit price on product change

    	//identify row and select box
            var idSelect          = this.id;
            
            var id                = idSelect[idSelect.length-1];
            
            var selected          = $(this).find('option:selected');
            
            //displaying unit price
            var unitPrice         = selected.data('foo');
            
            var unitPriceText     = '#unit-price';
            
            var unitPriceConcatId = unitPriceText.concat(id);

            
            $(unitPriceConcatId).val(unitPrice);
            
            //setting max quantity attribute and displaying stocks left
            var maxQty            = selected.data('qty');
            
            var maxQtyText        = '#quantity';
            
            var maxQtyConcatId    = maxQtyText.concat(id);
            
            var stocksLeft        = "#stocksLeft" + id;
            

            $(stocksLeft).text(maxQty);
            
            $(maxQtyConcatId).attr({
            "max" : maxQty,
            });
            
            //clear quantity when product is selected
            var qtyToClear        = '#quantity'+id;
            
            $(qtyToClear).val('');

 

	});

    $(wrapper).on('input', 'input', function(){
    
    	//used for calculating the total price per product and displaying it

        var inputIdText           = this.id;
        
        var inputId               = inputIdText[inputIdText.length-1];
        
        var inputQty              = this.value;
        
        var totalPriceText        = '#total-price'+ inputId;
        
        var unitPriceTextForTotal = '#unit-price'+ inputId;
        
        var unitPriceForTotal     = $(unitPriceTextForTotal).val();
        
        var totalProductPrice     = inputQty * unitPriceForTotal;

    	$(totalPriceText).val(totalProductPrice);

        calculateTotal();

	});

     function calculateTotal(){
        var netTotal = 0;

        $('.total-price').each(function(){
            var temp = $(this).val();
            if (isNaN(temp) || temp == ""){
                //this is for empty textboxes
                temp = 0;
            }
            var totalPrice = parseFloat(temp);
            netTotal = netTotal + totalPrice;
        })

        $('#total-cost-invoice').val(netTotal);
    }
});
	</script>
@endsection