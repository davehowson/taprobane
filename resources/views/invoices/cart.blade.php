@extends ('layouts.master')

@section ('title','Shopping Cart')

@section('content')
	<h3>Shopping Cart</h3>
	<div class="table-div">
		<table class="table">
			<thead>
				<tr>
					<th>Product Name</th>
					<th>Price</th>
					<th>Qty</th>
					<th>Total</th>
				</tr>
				@foreach ($lines as $line)
					<tr>
						<td>{{ $line->product->product_name }}</td>
						<td>{{ $line->product->product_price }}</td>
						<td>{{ $line->quantity }}</td>
						<td id="cost{{$line->id}}"></td>
					</tr>
				@endforeach
			</thead>
		</table>
		<div class="pull-right">
			<div>
                    <label for="total-cost-invoice-receipt">Total</label>
                    <input type="text" class="form-control total-cost" id="total-cost-invoice-receipt" readonly value="{{ $line->invoice->total_cost }}">
            </div>

			<form method="GET" action="/cart/{{ $invoice->id }}/cancel" >
				<button type="submit" class="btn btn-danger cart-button" >Cancel Invoice</button>
			</form>

			<form method="GET" action="#" >
				<button type="submit" class="btn btn-custom cart-button" >Proceed to Checkout</button>
			</form>
		</div>
	</div>
@stop

@section('script')

	<script>
		$(document).ready(function(){
			var lines = <?php echo json_encode($lines);?>;

			$.each(lines, function (i, elem) {
				var unitPrice = elem.unit_price;
				var qty       = elem.quantity;
				
				var cost      = unitPrice * qty;
				
				var id        = elem.id;
				
				var idText    = '#cost'+id;

    			$(idText).text(cost);
			});
		});
	</script>

@stop